# Demos Game Studio Club
## Python - Pygame

**Voici l'ensemble des démos implémentées en Python pour le Game Studio Club**

Version de Pygame utilisée pour les démos:
`pygame 2.0.1`

Installation de Pygame:
```
python3 -m pip install -U pygame --user
```
Voir [Pygame - Getting started](https://www.pygame.org/wiki/GettingStarted)

## Progression
**Pour une compréhension du code en douceur, l'ordre recommandé pour étudier les démos est le suivant :**
- naive_move_sprite
- simple_move_sprite
- bouncing_ball
- bouncing_multiball
- pong
- animated_sprite
- animated_sprite_collider
