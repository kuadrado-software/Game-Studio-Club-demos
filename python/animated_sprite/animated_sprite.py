"""
Dans cette démo nous allons voir comment associer des animations à un sprite, 
et comment les mettre en action à l'écran.
"""
import sys
import pygame
import random

from engine import Rectangle
from engine import MoveRequest
from engine import EventsHandler
from engine import FrameRateController
from engine import ResourcesManager
from engine import Render

from sprites import Runner

pygame.init()
# Définition d'un rectangle correspondant à la taille de la fenêtre que l'on veut
screenRect = Rectangle(pygame.Rect(0, 0, 1400, 800))

# Création de la fenêtre
screen = pygame.display.set_mode(screenRect.size())

# Création de l'objet eventHandler qui nous permettra de gérer les événements de clavier entre autre.
eventsHandler = EventsHandler()

# Déclaration d'un objet de type ResourcesManager.
# Commme son nom l'indique, le rôle de cet objet est la gestion des resources, notammement les images et animations.
resourcesManager = ResourcesManager()
# Chargeons les animations, après cette opération les animation seront utilisables sans plus avoir à se préocupper des
# fichiers.
resourcesManager.loadAnimations()

# Création d'un sprite de type Runner.
# Nous passons en paramètre de son constructeur la liste des animations qu'il peut utiliser
runner = Runner(resourcesManager.getAnimationSet("runner"))

# Création de l'objet moveRequest qui servira à enregistrer les appui sur les touches du clavier.
# Dans le cas présent nous ne souscrivons qu'à la touche flèche de droite pour un mouvement vers la droite.
moveRequest = MoveRequest()
moveRequest.subscribe({
    "right": pygame.K_RIGHT,
})

# Associons l'objet moveRequest à notre sprite
runner.setMoveRequestState(moveRequest)
# Et initialisons la position du runner à gauche et au centre de l'écran
runner.setPosition(0, screenRect.height / 2 -
                   runner.boundingBox.size()[1] / 2)

# Création de l'objet fpsController qui permettra de cadencer la vitesse de la boucle de rendu ci-dessous.
fpsController = FrameRateController(50)

while 1:  # Initialisation d'un boucle infinie
    if fpsController.nextFrameReady():  # On verifie si l'interval donné par le fpsController est écoulée
        # Si oui on calcule la frame suivante, si non on ne fait rien

        # On passe les événements au eventsHandler qui s'occupera de mettre à jour
        # l'objet moveRequest si une flèche à été appuyée ou relâchée.
        eventsHandler.handleEvtQueue(
            pygame.event.get(),
            (runner.moveRequestState,),
        )

        # Demandons au runner d'appliquer les requetes de mouvement qu'il a reçu
        runner.applyMoveRequestState()

        # Comme notre runner ne va que vers la droite, faisons le revenir à gauche de l'écran chaque fois qu'il sort du côté droit
        if runner.getPosition()[0] >= screenRect.width:
            runner.setPosition(
                -runner.boundingBox.width,
                runner.getPosition()[1]
            )

        # Puis appelons la méthode update du runner qui va se charger d'appliquer les différents changements necessaires
        # comme la position, l'animation etc.
        runner.update()

        Render.drawGame(  # On gère tout l'affichage avec une classe dédiée afin de rendre le code plus concis.
            screen,  # La surface sur laquelle dessiner
            (runner,),  # les sprites à dessiner (ici il n'y en a qu'un)
            (0, 0, 0)  # une couleur pour le fond de l'écran
        )
