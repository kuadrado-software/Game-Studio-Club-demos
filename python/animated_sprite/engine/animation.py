from .frame_rate import FrameRateController


class Animation:
    def __init__(self, animationData: dict):
        self.name = animationData["name"]
        self.frame_count = animationData["frame_count"]
        self.fps = animationData["fps"]
        self.image = animationData["image"]
        self.dimensions = animationData["dimensions"]
        # La position en x de la frame par rapport à l'ensemble des frames sur l'animation
        self.currentFrame = 0
        self.fpsController = FrameRateController(self.fps)

    def updateFrame(self):
        """
        Met à jour le numéro de la frame de l'animation qui doit être vue à l'instant t.
        """

        # le fpsController qui a été initialisé localement (dans le constructeur __init__ de l'objet)
        # nous permet de controller la vitesse à laquelle doit défiler l'animation
        # si le nombre de frame par seconde spécifié dans les données de l'animation est écoulé, on passe à la frame suivante
        if self.fpsController.nextFrameReady():

            # Cet algorihtme est un algorithme bitwise, cet à dire qu'il utilise des opérateur binaires.
            # L'avantage de ce genre d'algorithme est qu'il est très rapide à exécuter car les instructions
            # sont proches des opérations faites par le processeur et traitent directement les nombres dans leur
            # format binaires.
            # L'inconvénient est que c'est assez difficile à lire mais je vais tenter de l'expliquer :

            self.currentFrame = (
                (self.currentFrame + 1) &
                ~(self.currentFrame + 1 >= self.frame_count and ~0)
            )

            # Pour commencer il faut connaitre les opérateurs binaires utilisés ici, ainsi que comprendre les notions de bases d'un calcul
            # booléen (l'Algèbre de Boole est basé sur des calculs à partir de valeurs qui peuvent soit vrai soit faux, ou soit 1 soit 0)

            # & : l'opérateur bitwise ET permet d'associer deux valeurs vraies ou fausses et renvoie un résultat qui ne sera vrai que si
            # les deux valeurs sont vraies. Donc 1 & 1 = 1, 1 & 0 = 0, 0 & 1 = 0, 0 & 0 = 0. 
            # Mais les nombres binaires ont plusieurs chiffres donc il faut penser plutôt: 0101 & 0001 = 0001, 1111 & 0010 = 0010, etc...

            # ~ : l'opérateur "complément de 1" est un peu plus compliqué à utiliser mais sur le principe il est assez simple, il se
            # contente de remplacer les 0 par des 1 et les 1 par des 0. 
            # Il ne faut pas confondre cet opérateur avec un - (moins) ou avec un ! (negation logique).
            # Par exemple le nombre 5 en binaire s'écrit 0101. Donc ~5 donnera 1010, c'est à dire .. -6 ! Eh oui, le premier
            # chiffre en partant de la gauche est utilisé pour définir le signe du nombre, 0 pour + et 1 pour -. 
            # Bref, il ne faut pas utiliser cet opérateur pour faire des calculS décimaux comme nous en avons l'habitude mais il est 
            # très utile pour manipuler le binaire.

            # Les autres opérateurs sont des opérateurs booléens classiques (non binaire), nous avons >= (supérieur ou égal) et and (ET qui
            # fonctionne de la même façon que le & binaire).
            # La différence avec les opérateur binaires est que ceux ci ne renvoient pas
            # un nombre binaire mais simplement une valeur vrai ou faux. (donc en gros un nombre binaire à un seul chiffre)

            # Basiquement le but de l'opération est que self.currentFrame soit égal à lui même + 1 afin de passer à la frame suivante,
            # ou qu'il revienne à zero si nous sommes déjà à la dernièrement frame.

            # Donc la première partie de l'expression self.currentFrame + 1 vaudra au moins 1, En binaire, pour un entier codé sur 32bits
            # le nombre 1 s'écrire avec 31 zeros et un 1 au bout : 0000 0000 ... 0001

            # Ce 1 sera combiné avec & à l'inversion binaire de l'expression (currentFrame + 1 >= frame_count and ~0)

            # Ça se complique un peu mais allons y doucement:
            # currentFrame + 1 >= frame_count renverra vrai si nous sommes à la dernière frame. Ce que nous voulons c'est que l'ensemble de l'expression 
            # renvoie zero si ceci est vrai. 
            # Donc il faut que l'ensemble ~(self.currentFrame + 1 >= self.frame_count and ~0) renvoie soit 32bits de 1 si nous voulons que le 
            # résultat soit currentFrame + 1 ou renvoie 32bits de zeros si nous voulons zero.

            # En effet le fait de faire & entre un nombre d'un côté ett que des 1 de l'autre aura pour effet de renvoyer le nombre: 
            # 11010011 & 11111111 = 11010011
            # Et à l'inverse le fait de le combiner avec des zeros renverra zero : 
            # 11010011 & 00000000 = 00000000

            # Donc si on reprend: 
            # si self.currentFrame + 1 >= self.frame_count renvoie vrai, alors le and ~0 qui vient ensuite s'executera.
            # l'expression ~0 renvoie -1, c'est à dire une rangée complète de 1 (le fait de faire un & entre un nombre et une rangée de 1 
            # s'appelle un masque en binaire, car ça a pour effet de simplement copier le nombre).
            # Quand nous inversons cette expression comme ceci 
            # ~(currentFrame + 1 >= frame_count and ~0)
            # nous avons donc une rangée de zero si currentFrame + 1 >= frame_count est vrai, et une rangée de 1 si c'est faux. 
            # Nous aurons donc pour résultat currentFrame + 1 si currentFrame + 1 >= frame_count est faux et 0 si c'est faux.


            # Ça fait beaucoup de choses compliquées ! C'est normal. Afin de rendre la chose un peu lus digeste, voici un exemple d'algorithme possible
            # sans utiliser d'opérateurs binaires : 
            """
            if self.currentFrame + 1 >= self.frame_count:
                self.currentFrame = 0
            else:
                self.currentFrame = self.currentFrame + 1
            """
            

    def getFramePosition(self):
        """
        Renvoie la position en x de la frame à dessiner en fonction de l'index currentFrame.
        On se contente de multiplier ce nombre par la largeur d'une frame pour avoir la position.
        """
        return self.currentFrame * self.dimensions[0]

    def getFrameRect(self):
        """
        Renvoie un rectangle aux coordonées de la frame en cours.
        """
        w, h = self.dimensions
        return (self.getFramePosition(), 0, w, h)
