import pygame
from typing import Tuple
from .sprite import Sprite


class Render:
    """
    Expose des méthodes statiques chargées de l'affichage du jeu 
    """
    @staticmethod
    def drawGame(surface: pygame.display, sprites: Tuple[Sprite, ...], backgroundColor: Tuple[int, int, int]):
        """
        Methode publique qui appelle les méthodes __drawBackground et __drawSprites de la même classe
        Puis transfère les nouveaux pixels à l'écran grâce à pygame.display.flip()
        """
        Render.__drawBackground(surface, backgroundColor)
        Render.__drawSprites(surface, sprites)
        pygame.display.flip()

    @staticmethod
    def __drawBackground(surface, color):
        """
        Remplit la surface donnée avec la couleur passée en paramètre.
        """
        surface.fill(color)

    @staticmethod
    def __drawSprites(surface, sprites):
        """
        Dessine l'ensemble des sprites passés en paramètre sur la surface
        """
        for sprite in sprites:
            surface.blit(  # Les pixels sont passé à la surface grâce à la méthode blit()
                sprite.animation.image,
                sprite.getPosition(),
                sprite.animation.getFrameRect()
            )
