import pygame
from math import sqrt
from typing import Tuple

from .rectangle import Rectangle
from .move_request import MoveRequest
from .collisions import CollisionHandler
from .vectors import getUnitVec
from .animation import Animation


class Sprite:
    """
    La classe Sprite est un modèle "général" ou abstrait visant à généraliser les fonctionnalité dont
    peut avoir besoin n'importe quel sprite en général. Un sprite étant tout "objet" du jeu identifiable par une
    ou une animation, une position et un certain nombre de propriétés utiles (souvent de la physique).
    Ce modèle sera utilisé par des sous-classes qui hériteront des fonctionnalité de ce modèle et l'enchiront
    avec des valeurs concrètes

    Ci dessous sont définies les méthodes de la classe Sprite. C'est à dire que tous les objets définis à
    partir de ce modèle ou sur un modèle héritant de celui-ci auront avec eux les fonctions définies ici. 
    Dans chacune des fonctions il y a un paramètre "self" qui représente l'objet qui utilisera la fonction 
    ainsi que les différents attributs (image, boudingBox etc)
    """

    def __init__(self, animationSet: Tuple[Animation, ...]):
        """
        Les attributs de la classes sprite sont définies ici de façon abstraite, juste avec leur type.
        Par exemple l'attribut animation doit être un objet de la classe Animation, mais l'objet en lui 
        même sera défini dans une classe enfant (héritée de Sprite).
        """
        self.animation: Animation  # L'animation couramment utilisée par le sprite

        # L'ensemble des animations disponibles pour ce sprites seront référencées ici
        self.animationSet = animationSet

        self.boundingBox: Rectangle
        self.speed: float  # pixels par frame
        self.speedVector = (0.0, 0.0)
        self.moveRequestState: MoveRequest

    def getPosition(self):
        return self.boundingBox.getPosition()

    def setPosition(self, x, y):
        """
        Dans la programmation orientée objet (c'est ce qu'on fait ici, on crée des objets à partir de modèles qui relies des
        données et des fonctions, on crée des sous-modèles à partir d'autres modèles etc), c'est une bonne pratique de toujours
        faire en sorte que les objets ne soit pas modifiés directement par du code qui ne leur appartient pas. Ici par exemple
        on aurait pu imaginer directement assigner les valeur x et y dans les attributs correspondant de la boundingBox, mais
        au lieu de ça on lui envoie un message à travers une fonction en lui passant la valeur qu'on veut lui affecter, et c'est
        l'objet boundingBox qui se chargera lui même de s'affecter les valeur qu'on lui a donné. Ce principe s'appelle l'encapsulation
        et cela permet d'avoir un code plus sécurisé, moins sujet aux erreurs et aux bugs.
        """
        return self.boundingBox.setPosition(x, y)

    def setMoveRequestState(self, state):
        self.moveRequestState = state

    def setSpeed(self, speed: int):
        """
        Même principe que ce-dessus mais pour la vitesse (un nombre entier en frame par seconde)
        """
        self.speed = speed

    def setSpeedVector(self, unitVector):
        self.speedVector = self.getSpeedVecFromUnitVec(unitVector)

    def setAnimation(self, key):
        """
        Met à jour l'état d'animation courant pour ce sprite, en sélectionnant dans les animations
        disponible celle qui a un nom correspondant à la clé donnée et en l'assignant au champs self.animation.
        """
        for anim in self.animationSet:
            if anim.name == key:
                self.animation = anim
                self.updateBoundingBox()
                return

    def updateBoundingBox(self):
        """
        Recalcule le champs boundingBox à partir du rectangle de l'animation en cours.
        """
        _ix, _iy, iw, ih = self.animation.getFrameRect()
        px, py = self.boundingBox.getPosition() if hasattr(
            self, "boundingBox") else (0.0, 0.0)
        self.boundingBox = Rectangle(pygame.Rect((px, py, iw, ih)))

    def getSpeedVecFromUnitVec(self, vector: tuple) -> tuple:
        speedVec = (
            vector[0] * self.speed,
            vector[1] * self.speed
        )
        return speedVec

    def move(self):
        """
        La fonction move est appellée depuis la boucle de rendu, elle applique le vecteur de mouvement qui
        a été calculé sur les coordonnées de la position de l'objet. La position de l'objet étant contenue
        dans son attribut boundingBox qui est un rectangle (formaté avec la classe Rectangle) construit
        selon les dimensions de l'image utilisée pour l'objet (voir ball.py)
        """
        (x, y) = self.boundingBox.getPosition()
        (vx, vy) = self.speedVector
        self.boundingBox.setPosition(x + vx, y + vy)

    def applyBounceVector(self, vector: tuple):
        """
        Lorsque qu'un vecteur de rebond est calculé on le passe ici afin qu'il soit appliqué au vecteur de vitesse.
        L'opération qui a lieu ici est due au fait que le vecteur passé en paramètre est un vecteur unitaire (sa magnitude vaut 1)
        on doit donc le mutiplier avec la valeur absolue  de la composante correspondante dans le vecteur de vitesse réelle de l'objet
        L'opération booléenne est conçu de telle manière que si une des composante de "vector" vaut 0 c'est la deuxième opérande qui sera
        prise en compte (ce qui est après l'opérateur "or"), ici c'est en fait la valeur initiale de la composante.
        Pour résumer le vecteur passé en paramètre ne fait rien d'autre que d'appliquer un changement de signe (ou pas de changement) au
        vecteur de vitesse de notre objet. C'est donc un rebond très simple et complètement isométrique.
        """
        vx, vy = self.speedVector
        self.setSpeedVector(getUnitVec((
            # nous recalculons le vecteur de vitesse à partir de sa valeur unitaire, au cas ou la vitesse globale aurait changée
            vector[0] * abs(vx) or vx,
            vector[1] * abs(vy) or vy
        )))

    def applyMoveVector(self, vector: tuple):
        """
        Ici le vecteur qui est appliqué sur la vitesse correspond à un vecteur de mouvement plus classique, ce n'est pas un rebond.
        Le vecteur unitaire en paramètre donne une direction, et ici on se contente de multiplier ses composante avec la magnitude du
        vecteur vitesse. Ici cette magnitude est tout simplement un attribut speed avec une valeur constante. Dans une simulation de
        physique avec de l'accelération, de la friction etc, le calcul serait un peu plus élaboré.
        """
        self.setSpeedVector(vector)

    def bounceInside(self, rect: Rectangle):
        """
        Cette methode détermine si l'objet self est en collision avec une des parois du rectangle rect de l'intérieur
        Cela implique donc que l'objet self soit position à l'intérieur de rect.
        S'il y a collision, alors un vecteur de rebond sera calculé et directement appliqué
        La collision et le vecteur de rebond sont calculés par une méthode statique de la class CollisionHandler.
        Statique veut dire que la méthode est appelée directement sur la classe sans avoir besoin d'avoir créé
        un objet réel à partir d'elle. Voir explications dans collisions.py
        """
        self.applyBounceVector(
            CollisionHandler.rectBounceInsideRect(self.boundingBox, rect)
        )

    def bounce(self, rect: Rectangle):
        """Idem mais si self doit rebondir sur l'extérieur de rect"""
        if CollisionHandler.rectCollidesOutsideRect(self.boundingBox, rect):
            self.applyBounceVector(
                CollisionHandler.rectBounceOutsideRect(self.boundingBox, rect)
            )

    def collideInside(self, rect: Rectangle):
        """
        Les méthodes collideInside et collideOutside sont similaires aux méthodes bounce,
        sauf qu'au lieu de calculer un vecteur de rebond elles arrêtent simplement l'objet
        s'il entre en collision avec un des côtés de rect
        """
        self.applyMoveVector(
            CollisionHandler.rectCollideInertInsideRect(
                self.boundingBox, self.speedVector, rect
            )
        )

    def collideOutside(self, rect: Rectangle):
        if (CollisionHandler.rectCollidesOutsideRect(self.boundingBox, rect)):
            self.applyMoveVector(
                CollisionHandler.rectCollideInertOusiteRect(
                    self.boundingBox, self.speedVector, rect
                )
            )

    def applyMoveRequestState(self):
        """
        Cette méthode est appelée depuis la boucle de rendu afin de faire appliquer
        un état de requêtes de mouvement. C'est à dire que si on a appuyé sur des boutons pour faire
        bouger le sprite, ce sera enregistré dans l'objet moveRequest. Par exemple si pendant cette frame
        l'utilisateur appuie sur la fleche de gauche, alors moveRequest.left aura été mis égale à True (vrai)(i.e.
        Le calcul du vecteur passé dans la fonction applyMoveVector(...) est fait de manière à ce que :
        pour la composante x, si moveRequest.left est vrai l'expression renverra -1 (on ira donc vers la gauche),
        sinon on execute moveRequest.right grace à l'opérateur "|" qui est un OR binaire. Donc si moveRequest est vrai,
        l'expression renverra 1 (pour aller vers la droite), et si aucun des deux n'est vrai nous aurons 0 (pas de mouvement)
        Pour la composante y le fonctionnement est le même mais en se basant sur les champs up et down de l'objet moveRequest.
        """
        moveRequest = self.moveRequestState
        self.applyMoveVector((
            -(moveRequest.left) | moveRequest.right,
            -(moveRequest.up) | moveRequest.down
        ))

    def update(self):
        """
        Mets à jour la frame à dessiner à l'instant T pour ce sprite.
        Calcule le vecteur de mouvement
        Mets à jour la position du sprite.
        """
        self.animation.updateFrame()
        self.move()
