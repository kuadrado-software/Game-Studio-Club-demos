import pygame
from engine.collisions import CollisionHandler
from engine import Rectangle


class TestCollisionHandler:
    """
    Unit tests for the CollisionsHandler class
    """
    def test_rectCollidesOutsideRect(self):
        """
        Test collision of r1 outside of r2
        """
        r1 = Rectangle(pygame.Rect((0, 0, 20, 20)))
        r2 = Rectangle(pygame.Rect((19, 15, 30, 40)))
        r3 = Rectangle(pygame.Rect((150, 300, 50, 50)))

        assert CollisionHandler.rectCollidesOutsideRect(
            r1, r2) == True
        assert CollisionHandler.rectCollidesOutsideRect(r1, r3) == False

    def test_rectCollidesInsideRect(self):
        """
        Test collision of r1 inside of r2
        """
        r1 = Rectangle(pygame.Rect(0, 0, 300, 200))
        r2 = Rectangle(pygame.Rect((0, 27, 50, 50)))
        r3 = Rectangle(pygame.Rect((100, 70, 40, 40)))

        assert CollisionHandler.rectCollidesInsideRect(
            r2, r1) == True
        assert CollisionHandler.rectCollidesInsideRect(r3, r1) == False

    def test_rectBounceInsideRect(self):
        """
        Test bounce vector calculation when top left right and bottom colliders
        collides inside container rectangle.
        """
        container = Rectangle(pygame.Rect(0, 0, 300, 200))
        topCollider = Rectangle(pygame.Rect(35, -1, 20, 20))
        leftCollider = Rectangle(pygame.Rect(0, 150, 20, 20))
        rightCollider = Rectangle(pygame.Rect((250, 70, 50, 10)))
        bottomCollider = Rectangle(pygame.Rect((200, 280, 21, 21)))

        assert CollisionHandler.rectBounceInsideRect(
            topCollider, container) == (0, 1)
        assert CollisionHandler.rectBounceInsideRect(
            leftCollider, container) == (1, 0)
        assert CollisionHandler.rectBounceInsideRect(
            rightCollider, container) == (-1, 0)
        assert CollisionHandler.rectBounceInsideRect(
            bottomCollider, container) == (0, -1)

    def test_rectBounceOutsideRect(self):
        """
        Test bounce vector calculation when top left right and bottom colliders
        collides outside container rectangle.
        """
        r = Rectangle(pygame.Rect((50, 50, 50, 50)))
        topCollider = Rectangle(pygame.Rect(60, 40, 10, 10))
        rightCollider = Rectangle(pygame.Rect(100, 60, 10, 10))
        leftCollider = Rectangle(pygame.Rect((41, 70, 10, 10)))
        bottomCollider = Rectangle(pygame.Rect((70, 100, 10, 10)))

        assert CollisionHandler.rectCollidesOutsideRect(
            topCollider, r) and CollisionHandler.rectBounceOutsideRect(topCollider, r) == (0, -1)
        assert CollisionHandler.rectCollidesOutsideRect(
            leftCollider, r) and CollisionHandler.rectBounceOutsideRect(leftCollider, r) == (-1, 0)
        assert CollisionHandler.rectCollidesOutsideRect(
            rightCollider, r) and CollisionHandler.rectBounceOutsideRect(rightCollider, r) == (1, 0)
        assert CollisionHandler.rectCollidesOutsideRect(
            bottomCollider, r) and CollisionHandler.rectBounceOutsideRect(bottomCollider, r) == (0, 1)

    def test_rectCollideInertOusiteRect(self):
        """
        Test inert stopping vector calculation when movingRect collides ouside
        the obstacle rectangle with an oposite movement
        """
        obstacle = Rectangle(pygame.Rect((20, 20, 50, 50)))

        movingRect = Rectangle(pygame.Rect((11, 30, 10, 10)))
        moveRightVector = (5.2, 0)
        assert CollisionHandler.rectCollideInertOusiteRect(
            movingRect, moveRightVector, obstacle) == (0, 0)

        movingRect = Rectangle(pygame.Rect((70, 30, 10, 10)))
        moveLeftVector = (-3, 0)
        assert CollisionHandler.rectCollideInertOusiteRect(
            movingRect, moveLeftVector, obstacle) == (0, 0)

        movingRect = Rectangle(pygame.Rect((30, 10, 10, 10)))
        movingDownVector = (0, 4)
        assert CollisionHandler.rectCollideInertOusiteRect(
            movingRect, movingDownVector, obstacle) == (0, 0)

        movingRect = Rectangle(pygame.Rect((30, 69, 10, 10)))
        movingUpVectop = (0, -2)
        assert CollisionHandler.rectCollideInertOusiteRect(
            movingRect, movingUpVectop, obstacle) == (0, 0)

    def test_rectCollideInertInsideRect(self):
        """
        Test inert stopping vector calculation when movingRect collides inside
        the obstacle rectangle with an oposite movement
        """
        obstacle = Rectangle(pygame.Rect((0, 0, 100, 100)))

        movingRect = Rectangle(pygame.Rect((91, 30, 10, 10)))
        moveRightVector = (5.2, 0)
        assert CollisionHandler.rectCollideInertInsideRect(
            movingRect, moveRightVector, obstacle) == (0, 0)

        movingRect = Rectangle(pygame.Rect((0, 30, 10, 10)))
        moveLeftVector = (-3, 0)
        assert CollisionHandler.rectCollideInertInsideRect(
            movingRect, moveLeftVector, obstacle) == (0, 0)

        movingRect = Rectangle(pygame.Rect((50, 90, 10, 10)))
        movingDownVector = (0, 4)
        assert CollisionHandler.rectCollideInertInsideRect(
            movingRect, movingDownVector, obstacle) == (0, 0)

        movingRect = Rectangle(pygame.Rect((50, -1, 10, 10)))
        movingUpVectop = (0, -2)
        assert CollisionHandler.rectCollideInertInsideRect(
            movingRect, movingUpVectop, obstacle) == (0, 0)
