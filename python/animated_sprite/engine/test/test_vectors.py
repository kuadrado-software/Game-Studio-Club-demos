from engine.vectors import getUnitVec


def test_getUnitVec():
    """
    Test if the vector returned by the getUnitVec function has magnitude of 1
    """
    test_vecs = (
        (35, -22),
        (-1, 0),
        (0, 65),
        (-10, -20)
    )
    for vec in test_vecs:
        unitVec = getUnitVec(vec)
        # Round is necessary because we may get a floating point very close of 1 instead of 1
        assert round(unitVec[0] ** 2 + unitVec[1] ** 2) == 1
