import pygame
from engine import Rectangle
from engine import Sprite
from engine.collisions import CollisionHandler
from engine.vectors import getUnitVec


class Runner(Sprite):
    """
    Définit un objet Runner (coureur) héritant de Sprite
    """

    def __init__(self, animationSet):
        """
        Appelle le constructeur  parent (Sprite())
        Initialise la vitesse du sprite
        Initialise son set d'animations possibles et un état initial d'animation
        """
        super().__init__(animationSet)
        self.speed = 15
        self.animationSet = animationSet
        self.setAnimation("run")

    def update(self):
        """
        Compléte la méthode update définie par héritage dans la classe Sprite.
        Nous lui ajoutons un appel à la méthode updateAnimation() avant d'appeler
        l'implémentation héritée grâce à super().update().
        super() designe la classe parente, ici Sprite. et nous appelons dessus la méhode 
        update que nous avons complété ici.
        """
        self.updateAnimation()
        super().update()

    def updateAnimation(self):
        """
        Met à jour l'animation à utiliser en fonction du mouvement du sprite

        Dans cette démo notre runner ne fait que aller vers la droite ou se tenir à l'arret, nous ne prenons donc en compte que ces deux cas.
        """

        # récupérons la valeur horizontale du vecteur de mouvement du sprite et assignons la à une variables locales vx
        vx = self.speedVector[0]

        if vx > 0:
            self.setAnimation("run")
        elif vx == 0:
            self.setAnimation("stand")
