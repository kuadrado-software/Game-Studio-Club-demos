"""
Cette démo est similaire à la démo animated_sprite, mais nous allons ici ajouter la notion de collider,
afin de pouvoir gérer des collisions entre sprite en utilisant des rectangles de différentes dimensions.
"""

import sys
import pygame
import random

from engine import Rectangle
from engine import MoveRequest
from engine import EventsHandler
from engine import FrameRateController
from engine import ResourcesManager
from engine import Render

from sprites import Runner

pygame.init()
# Définition d'un rectangle correspondant à la taille de la fenêtre que l'on veut
screenRect = Rectangle(pygame.Rect(0, 0, 1400, 800))

# Création de la fenêtre
screen = pygame.display.set_mode(screenRect.size())

# Création de l'objet eventHandler qui nous permettra de gérer les événements de clavier entre autre.
eventsHandler = EventsHandler()

# Déclaration d'un objet de type ResourcesManager.
# Commme son nom l'indique, le rôle de cet objet est la gestion des resources, notammement les images et animations.
resourcesManager = ResourcesManager()
# Chargeons les animations, après cette opération les animation seront utilisables sans plus avoir à se préocupper des
# fichiers.
resourcesManager.loadAnimations()

# Création d'un sprite de type Runner.
# Nous passons en paramètre de son constructeur la liste des animations qu'il peut utiliser
runner = Runner(resourcesManager.getAnimationSet("runner"))

# Création de l'objet moveRequest qui servira à enregistrer les appui sur les touches du clavier.
# Dans cette démo le sprite runner ne pourra pas sortir de l'écran, donc afin que ce soit un peu plus
# Nous allons en profiter pour implémenter la possibilité de pouvoir le faire aller dans les deux directions.
# Pour ce faire, la classe Runner à été modifiée pour prendre en compte l'animation vers la gauche.
# Et la classe Animation a également été ajustée pour permettre l'affichage d'une animation
# avec une transformation en miroir selon ce qui est spécifié.
moveRequest = MoveRequest()
moveRequest.subscribe({
    "right": pygame.K_RIGHT,
    "left": pygame.K_LEFT
})

# Associons l'objet moveRequest à notre sprite
runner.setMoveRequestState(moveRequest)
# Et initialisons la position du runner à gauche et au centre de l'écran
runner.setPosition(0 - runner.getCollider().xywh()[0], screenRect.height / 2 -
                   runner.boundingBox.size()[1] / 2)

# Création de l'objet fpsController qui permettra de cadencer la vitesse de la boucle de rendu ci-dessous.
fpsController = FrameRateController(50)

while 1:  # Initialisation d'un boucle infinie
    if fpsController.nextFrameReady():  # On verifie si l'interval donné par le fpsController est écoulée
        # Si oui on calcule la frame suivante, si non on ne fait rien

        # On passe les événements au eventsHandler qui s'occupera de mettre à jour
        # l'objet moveRequest si une flèche à été appuyée ou relâchée.
        eventsHandler.handleEvtQueue(
            pygame.event.get(),
            (runner.moveRequestState,),
        )

        # Demandons au runner d'appliquer les requetes de mouvement qu'il a reçu
        runner.applyMoveRequestState()

        # Ajoutons une instruction de collision à notre runner afin qu'il ne puisse plus sortir de l'écran.
        # Contrairement aux démo précédente, cette fois-ci les collisions vont prendre en compte la notion de
        # collider. Un collider est simplement un rectangle (du moins ici, dans un jeu plus abouti il pourrait
        # être n'importe quel polygone, ou même un volume pour un jeu en 3D), mais ce rectangle n'est pas forcément
        # égal à la taile totale de l'animation. On définit sa taille dans notre base de données d'animation (
        # resources/animations/index.json ) dont se sert notre resourceManager pour charger les fichiers.
        # Pour voir le code correspondant à cette notion, rendez-vous dans sprite.py et dans animation.py.
        # Pour définir la dimension du collider que vous voulez renseigner dans le index.json pour votre animation,
        # le plus facile est de la visualiser avec un logiciel de traitement d'image.
        runner.collideInside(screenRect)

        # Puis appelons la méthode update du runner qui va se charger d'appliquer les différents changements necessaires
        # comme la position, l'animation etc.
        runner.update()

        Render.drawGame(  # On gère tout l'affichage avec une classe dédiée afin de rendre le code plus concis.
            screen,  # La surface sur laquelle dessiner
            (runner,),  # les sprites à dessiner (ici il n'y en a qu'un)
            (0, 0, 0)  # une couleur pour le fond de l'écran
        )
