import pygame
import os
from engine.animation import Animation
from engine.resources_manager import ResourcesManager


class TestResourcesManager:
    """
    Unit tests for the ResourcesManager class
    """
    rm = ResourcesManager()

    def test_loadAnimations(self):
        """
        Test the ResourcesManager.loadAnimation method
        """
        rm = TestResourcesManager.rm
        rm.loadAnimations()
        assert len(rm.animations)
        for key in rm.animations.keys():
            keyAnimations = rm.animations.get(key)
            for anim in keyAnimations:
                assert isinstance(anim, Animation)
                assert isinstance(anim.image, pygame.Surface)

    def test_getAnimationSet(self):
        """
        Test the ResourcesManager.getAnimationSet method
        """
        rm = TestResourcesManager.rm
        rm.loadAnimations()
        animSet = rm.getAnimationSet("test")
        assert len(animSet) > 0
        for anim in animSet:
            assert isinstance(anim, Animation)
            assert isinstance(anim.image, pygame.Surface)
