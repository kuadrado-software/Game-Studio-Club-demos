# Ce programme est un peu plus structuré que les précédents, les classes correspondants aux sprites ont
# été rangées dans un module sprites (voir sous-dossier boucing_ball/sprites/). Les fonctionnalité génériques
# liées aux mécanisme du jeu ont été placée dans un module engine/, ce qui permet de rendre le fichier principal
# (celui-ci, le point d'entrée de l'exécution du code) plus concis.

import sys
import pygame

from engine import Rectangle
from engine import MoveRequest
from engine import EventsHandler
from engine import FrameRateController

from sprites import Ball
from sprites import Brick

# Initialisation de pygame
pygame.init()

# Définition d'un rectangle correspondant à la taille de la fenêtre que l'on veut
screenRect = Rectangle(pygame.Rect(0, 0, 1200, 800))

# La couleur que nous utiliserons pour le fond d'écran
black = 0, 0, 0

# Création de la fenêtre
screen = pygame.display.set_mode(screenRect.size())

# Création du sprite ball et initialisation de sa vitesse. Par défaut sa
# position sera (0, 0)
ball = Ball()
ball.setSpeedVector((1, 1))

# Création du sprite brick et initialisation de sa position.
# Par défaut son vecteur de vitesse sera (0.0, 0.0)
brick = Brick()
brick.setPosition(screenRect.width / 2 - brick.boundingBox.width / 2,
                  screenRect.height / 2 - brick.boundingBox.height / 2)

# Création de l'objet eventHandler qui nous permettra de gérer les événements de clavier entre autre.
eventsHandler = EventsHandler()

# création de l'objet MoveRequest qui nous permettra de stocker de façon persistante les requêtes
# de mouvement qui sont faîtes lorsque l'on appuie sur les flèche du clavier.
moveRequest = MoveRequest()

# Création de l'objet fpsController qui permettra de cadencer la vitesse de la boucle de rendu ci-dessous.
fpsController = FrameRateController(50)

while 1:
    if fpsController.nextFrameReady():  # On verifie si l'interval donné par le fpsController est écoulée
        # Si oui on calcule la frame suivante

        # On passe les événements au eventsHandler qui s'occupera de mettre à jour l'objet moveRequest si une flèche à été
        # appuyée ou relâchée.
        eventsHandler.handleEvtQueue(pygame.event.get(), moveRequest)

        # On calcule la collision et le rebond de la balle sur le bord de l'écran, si la balle ne touche pas le bord de l'écran
        # il ne se passera rien dans cette frame.
        ball.bounceInside(screenRect)
        # On calcule la collision et le rebond de la balle sur la brique
        ball.bounce(brick.boundingBox)

        # La brique est le sprite que nous choisissons de faire obéir à nos requête de mouvement.
        # On va donc le faire bouger en fonction des touches que l'on a appuyées.
        brick.applyMoveRequest(moveRequest)

        # La brique doit collisionner à l'intérieur de l'écran, afin qu'elle ne puisse pas sortir du cadre.
        brick.collideInside(screenRect)

        # On applique les vecteurs de mouvement calculés pour la balle et la brique à leurs positions respectives.
        ball.move()
        brick.move()

        # On repeint le fond en noir
        screen.fill(black)

        # Et on redessine nos objets à leur nouvelle position
        screen.blit(ball.image, ball.getPosition())
        screen.blit(brick.image, brick.getPosition())

        # On transfère les pixels de la fenêtre à l'affichage matériel, les nouveaux remplacent les anciens,
        # les anciens sont supprimés pour laisser leur place en mémoire.
        pygame.display.flip()
