class MoveRequest:
    """
    La classe MoveRequest est un modèle d'objet qui a pour but
    de stocker un état de requête de mouvement vers le haut la
    droite la gauche ou le bas. C'est une structure très simple
    avec 4 champs qui permets simplement de pouvoir retrouver ensemble
    ces 4 valeurs en mémoire de façon pratique.
    """

    def __init__(self):
        self.up = False
        self.right = False
        self.down = False
        self.left = False

    def setState(self, key: str, value: bool):
        """
        La classe MoveRequest expose une méthode setState dont le rôle est de mettre à jour l'état de l'objet 
        afin que celui-ci n'aie pas besoin d'être transormé directement de l'extérieur (principe 
        d'encapsulation des objets, cela évite des erreurs, des bugs, des fuites de mémoires etc.)
        Cet objet est conçu pour répondre à des requêtes directionnelles assez simples, gauche droite bas haut,
        qui correspondent typiquement à l'appui sur les flèches du clavier pour faire bouger un sprite.
        """
        if key == "ignore":
            return
        setattr(self, key, value)
