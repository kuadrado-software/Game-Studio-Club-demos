import pygame


class Rectangle:
    """
    La classe Rectangle va nous servir à généraliser la notion de rectangle
    en rangeant ensemble des valeurs pratiques comme les coordonées des
    côtés gauche, droit, haut et bas, et également la largeur et la hauteur.
    Des méthodes sont exposé afin de récupérer des séries de valeurs précises
    getPosition pour avoir juste les coordonées x y (correspondant au coin supérieur
    gauche du rectangle), ltrb (pour left top right bottom) pour un tuple de coordonées
    gauche haut droite bas, xywh (x y width height) pour x y largeur hauteur, et size pour
    avoir juste la largeur et la hauteur.
    Il y a également la méthode setPosition qui permet de mettre à jour les coordonnées de
    l'objet à partir de deux valeur x et y.

    En fait la bibliothèque pygame propose déjà une généralisation de rectangle dans ses classes.
    C'est d'ailleur de ce modèle que nous partons pour construire celui ci. L'intérêt d'avoir le
    nôtre est que le modèle de pygame ne supporte pas les nombres à virgule (float) dans ses coordonnées
    ce qui est un peu limitant pour pouvoir être précis sur des calculs de vitesse, d'acceleration, etc.
    """

    def __init__(self, rectData: pygame.Rect):
        self.left = float(rectData.left)
        self.top = float(rectData.top)
        self.right = float(rectData.right)
        self.bottom = float(rectData.bottom)
        self.width = float(rectData.width)
        self.height = float(rectData.height)

    def getPosition(self):
        return (self.left, self.top)

    def setPosition(self, x, y):
        self.left = x
        self.top = y
        self.right = self.left + self.width
        self.bottom = self.top + self.height

    def ltrb(self):
        return (self.left, self.top, self.right, self.bottom)

    def xywh(self):
        return (self.left, self.top, self.width, self.height)

    def size(self):
        return (int(self.width), int(self.height))
