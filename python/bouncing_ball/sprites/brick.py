import pygame
from engine import Rectangle
from engine import Sprite


class Brick(Sprite):
    """
    Tout comme la classe Ball la classe Brick hérite de la classe Sprite mais on
    l'initialise avec des valeurs un peu différentes dans ses attributs (image et vitesse)
    """

    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("resources/images/brick.png")
        self.boundingBox = Rectangle(self.image.get_rect())
        self.speed = 1.5
