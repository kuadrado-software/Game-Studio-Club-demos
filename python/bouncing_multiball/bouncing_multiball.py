# Cette démo est similaire à la démo boucing ball excepté que nous créer 10 balles
# à des endroits aléatoires au lieu d'une seule.

import sys
import pygame
import random

from engine import Rectangle
from engine import MoveRequest
from engine import EventsHandler
from engine import FrameRateController

from sprites import Ball
from sprites import Brick


pygame.init()

screenRect = Rectangle(pygame.Rect(0, 0, 1200, 800))
black = 0, 0, 0

screen = pygame.display.set_mode(screenRect.size())

# On crée 10 objets Ball dans une liste. Cette syntaxe s'appelle
# une "compréhension de liste" en Python, elle est très pratique
# et permet de créer des listes d'éléments à partir d'une boucle et d'une
# expression dynamique le tout en une seule expression.

balls = [
    Ball() for ball in range(10)
]

# Maintenant que les 10 objets Ball sont créés, nous allons initialiser leur
# position pour chacune d'elles à des coordonnées aléatoires sur la surface de
# la fenêtre.
# Nous allons également initialiser une vecteur (vx vy) pour le mouvement de chaque balle
for ball in balls:
    # random.randint est ue fonction du langage Python qui permet de générer
    # un nombre entier compris entre les 2 valeurs passées en paramètre
    # (ici entre 0 et la largeur de l'écran - la largeur de la balle)
    x = random.randint(0, screenRect.width - ball.boundingBox.width)
    y = random.randint(0, screenRect.height - ball.boundingBox.height)
    ball.setPosition(x, y)

    # Afin d'avoir un effet plus amusant, initialisons le mouvement des
    # balles dans des directions différentes. Pour cela on va choisir
    # aléatoirement le signe de chaque composantes du vecteur. Ainsi chaque
    # balle ira soit à en haut à droite soit en haut à gauche soit en bas à droite
    # soit en bas à gauche. Jusqu'à ce qu'elle rebondisse...

    # Commençons par donner à chaque balle une vitesse différente entre 2 et 8 pixels par frame
    ball.setSpeed(random.randint(2, 8))
    vx = random.choice((1, -1))
    vy = random.choice((1, -1))
    ball.setSpeedVector((vx, vy))


brick = Brick()

# Initialiser la brique au centre de l'écran
brick.setPosition(screenRect.width / 2 - brick.boundingBox.width / 2,
                  screenRect.height / 2 - brick.boundingBox.height / 2)

eventsHandler = EventsHandler()

moveRequest = MoveRequest()

fpsController = FrameRateController(50)

while 1:
    if fpsController.nextFrameReady():
        eventsHandler.handleEvtQueue(pygame.event.get(), moveRequest)

        screen.fill(black)

        # Nous faisons ici une boucle pour parcourir l'ensemble des 10 balles
        # afin d'appliquer le même traitement à chaque balle.
        # Chaque balle doit rebondir sur le bord de l'écran si elle le touche
        # Puis chaque balle doit rebondir sur la brique
        # Puis chaque balle applique le mouvement qu'elle a calculé
        # Puis on pass les pixels de chaque balle à l'écran avec blit()
        for ball in balls:
            ball.bounceInside(screenRect)
            ball.bounce(brick.boundingBox)
            # Faisons également en sorte que les balles rebondissent les unes sur les autres
            # Pour cela il faut refaire une boucle sur l'ensemble des balles pour chaque balle
            # (dans un vrai jeu il faudrait imaginer une algorithme moins consommateur que de re parcourir
            # l'ensemble des sprites pour chaque sprite ...)
            for b in balls:
                if b != ball:  # Il faut faire attention de ne pas dire à une balle de rebondire sur elle même !
                    ball.bounce(b.boundingBox)
            ball.move()
            screen.blit(ball.image, ball.getPosition())

        brick.applyMoveRequest(moveRequest)
        brick.collideInside(screenRect)
        brick.move()
        screen.blit(brick.image, brick.getPosition())

        pygame.display.flip()
