from time import time


class FrameRateController:
    """
    La classe FrameRateController a pour rôle de cadencer la vitesse de rafraîchissement
    de la boucle de rendu du jeu(le while: 1 qui est dans le fichier principal).
    Si on ne mettait pas en place cette limite, la boucle tournerait à son maximum ce qui est tentant
    car la fluidité des animation sera supérieure. Mais cela poserait également quelques problèmes.
    En effet la vitesse de la boucle et la durée de chaque frame dépend de la capacité du processeur
    à effectuer des calculs en un temps très court. Donc si on ne cadence pas la fréquence, une frame
    pourrait être plus longue à s'écouler si elle a plus de calcul à faire qu'une autre. Et surtout
    si jamais le jeu est exécuté sur un ordinateur plus lent, le jeu semblera aller au ralenti !
    Donc l'intérêt de mettre en place une limite, à partir du moment ou cette limite est suffisament
    basse pour être supportée par un ordinateur raisonnablement lent, c'est que le jeu se comportera
    de la même manière sur tous les ordinateurs, et il n'y aura pas de variations (ou alors beaucoup moins)
    dans la fluidité des animation, toutes les frames auront à peu près la même durée.
    """

    def __init__(self, fps):
        """
        Un objet de la classe FrameRateController sera initialisé avec une valeur fps (frames par seconde)
        qui déterminera la cadence de rafraîchissement (interval).
        """
        self.tframe = FrameRateController.getTime()
        self.interval = 1000 / fps

    @staticmethod
    def getTime():
        """
        Cette fonction statique nous donne l'heure à la milliseconde près !
        Nous l'utiliserons pour mesurer le temps écoulé dans chaque frame
        """
        return int(time() * 1000)

    def nextFrameReady(self) -> bool:
        """
        Cette méthode est celle qui est appelée à chaque nouveau tour de la boucle
        de rendu pour savoir si suffisament de temps s'est écoulé pour pouvoir calculer
        la frame suivante. Si le temps n'est pas écoulé, la boucle tournera à vide jusqu'à
        ce que "ready" soit vrai.

        - on note l'heure de l'instant en millisecondes
        - on regarde combien de temps s'est écoulé depuis la dernière fois qu'on a noté l'heure
        - On définit la valeur ready à vrai si le temps écoulé est supérieur à l'interval, sinon, faux.
        - Si l'interval est écoulé, on note l'heure du moment dans le champs tframe de l'objet afin de 
        pouvoir le comparer à nouveau dans la frame suivante.
        - Puis on renvoie le résultat qui sera utilisé comme condition dans la boucle principale.
        """
        now = FrameRateController.getTime()
        elpased = now - self.tframe
        ready = elpased > self.interval
        if ready:
            self.tframe = now - (elpased % self.interval)
        return ready
