import pygame
from engine import Rectangle
from engine import Sprite


class Ball(Sprite):
    """
    La classe Ball est une extension de la classe Sprite. L'héritage est spécifié avec la notation
    Ball(Sprite) qui définit donc un modèle d'objet Ball héritant d'un modèle Sprite.
    La objet de la classe Ball contiendra tous les attributs et les méthodes définis par la
    classe Sprite et a la possibilité de les redéfinir à volonté.
    Ici la classe ball ne fait que définir les champs image, boudingBox et speed qui ne sont
    définis dans la classe Sprite que par leur type.
    """

    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("resources/images/ball.png")
        self.boundingBox = Rectangle(self.image.get_rect())
        self.speed = 4
