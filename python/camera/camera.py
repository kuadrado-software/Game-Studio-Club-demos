import sys
import os
import json
import pygame

from engine import (
    Rectangle,
    MoveRequest,
    EventsHandler,
    FrameRateController,
    ResourcesManager,
    Render,
    Camera
)

from game.sprites import Bonom, Chalet, Mare, Arbre
from game import BigMap

pygame.init()

screenRect = Rectangle(pygame.Rect(0, 0, 1000, 600))
screen = pygame.display.set_mode(screenRect.size())

eventsHandler = EventsHandler()


resourcesManager = ResourcesManager()
resourcesManager.loadAnimations()

bigMap = BigMap(resourcesManager.getAnimationSet("big_map"))

bonom = Bonom(resourcesManager.getAnimationSet("bonom"))
moveRequest = MoveRequest()
moveRequest.subscribe({
    "up": pygame.K_UP,
    "down": pygame.K_DOWN,
    "right": pygame.K_RIGHT,
    "left": pygame.K_LEFT
})
bonom.setMoveRequestState(moveRequest)
bonom.setPosition(bigMap.rect.width / 2, bigMap.rect.height / 2)

gameObjectsData = open(os.path.join("game", "sprites", "game_objects.json"))
gameObjectsData = json.load(gameObjectsData)
spriteTypes = {
    "chalet": Chalet,
    "mare": Mare,
    "arbre": Arbre
}
sprites = list()
for spriteType in gameObjectsData.keys():
    for spritePos in gameObjectsData[spriteType]:
        sprite = spriteTypes.get(spriteType)(
            resourcesManager.getAnimationSet(spriteType))
        sprite.setPosition(spritePos[0], spritePos[1])
        sprites.append(sprite)

camera = Camera(
    Rectangle(pygame.Rect((0, 0), screen.get_size())),
    bigMap.rect
)

fpsController = FrameRateController(50)

while 1:
    if fpsController.nextFrameReady():
        eventsHandler.handleEvtQueue(
            pygame.event.get(),
            (bonom.moveRequestState,),
        )

        bonom.applyMoveRequestState()
        bonom.collideInside(bigMap.rect)

        for sprite in sprites:
            bonom.collideOutside(sprite.getCollider())

        bonom.update()

        camera.follow(bonom.getCenterPosition())

        Render.drawGame(
            screen,
            (*sprites, bonom),
            bigMap.image,
            (-camera.position[0], -camera.position[1])
        )
