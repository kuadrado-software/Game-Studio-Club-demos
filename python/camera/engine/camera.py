from typing import Tuple
from .rectangle import Rectangle
from .collisions import CollisionHandler


class Camera:
    def __init__(self, focus: Rectangle, bounds: Rectangle):
        self.bounds = bounds
        self.focus = focus
        self.position = (0, 0)

    def follow(self, position: Tuple[int, int]):
        camPos = [
            position[0] - self.focus.width / 2,
            position[1] - self.focus.height / 2
        ]

        if camPos[0] < 0:
            camPos[0] = 0
        elif camPos[0] + self.focus.width > self.bounds.right:
            camPos[0] = self.bounds.right - self.focus.width

        if camPos[1] < 0:
            camPos[1] = 0
        elif camPos[1] + self.focus.height > self.bounds.bottom:
            camPos[1] = self.bounds.bottom - self.focus.height

        self.position = camPos
