from math import copysign
from .rectangle import Rectangle


class CollisionHandler:
    """
    La classe CollisionHandler ne contient que des méthodes statiques.
    Les méthodes statiques d'une classe peuvent être appelées directement
    dans la classe, elle n'ont pas besoin d'appartenir à un objet instancié.
    Par exemple si je définit une classe Peronnage comme ceci
    ```
    class Personnage:
        def marcher(self):
            ...
    ```
    la méthode marcher sera accessible pour les objets de la classe Personnage.
    Pour avoir un objet de la classe Personnage je dois d'abord l'instancier comme ceci
    perso = Personnage()
    Ici j'ai déclaré une variable et fait un appel sur la classe Personnage comme si il s'agissait
    d'une fonction. En réalité j'ai effectivement appelé une fonction. Cette fonction est implicite
    et est appelé un constructeur. Elle peut aussi être redéfinie afin de spécifier des choses à faire
    lors de l'instanciation d'un objet:
    ```
    class Personnage:
        def __init__(self):
            Instruction au moment de l'instanciation...
         def marcher(self):
             Instruction pour faire marcher le personnage
    ```

    Mais une classe python permets aussi d'exposer des méthodes statiques qui ne seront pas embarquée dans
    les objets instanciés et devront être appelées directement sur la classe:
    ```
    class Personnage:
        def marcher(self):
            ...

        @staticmethod
        def staticFunction(self):
            return "coucou je suis statique"
    ```

    Dans cet exemple si j'instancie un objet Personnage il n'embarquera pas
    la fonction staticFunction car elle est définie statiquement. Par contre
    je peux l'appeler sur la classe depuis n'importe ou.
    ```
    perso = Personnage()
    perso.marcher() # ok
    perso.staticFunction() # Erreur
    message = Personnage.staticFunction() # ok
    print(message) # ceci écrira dans la console "coucou je suis statique"
    ```
    Certaines classes, comme celle ci-dessous peuvent ne contenir que des fonctions
    statiques, et ne sont donc pas destinées à instancier des objets. Ce sont simplement
    des paquets de stockage pour des fonctions qui ont intérêt à être rangées ensemble.

    La classe CollisionHandler définit un certain nombre de méthode destinées à détecter
    des collisions entre rectangles et à calculer le vecteur de mouvement en conséquence de la collision.
    """

    @staticmethod
    def rectCollidesOutsideRect(r1: Rectangle, r2: Rectangle) -> bool:
        """
        Cette méthode renvoie une valeur booléenne (soit vrai soit faux), selon si
        le rectangle r1 est en contact avec le rectangle r2, à l'extérieur.
        Des valeurs de coordonnées left, right, top, bottom (gauche, droite, haut, bas) sont
        extraites de la fonction ltrb() de la class Rectangle
        Puis un calcul booléen est renvoyé de la fonction en enchainant enssemble toutes les conditions
        pour que la collision soit vraie (le chainage est fait avec des opérateurs logiques binaires, ou bitwise)
        Le résultat sera donc True en cas de collision et False le reste du temps.
        """
        l1, t1, r1, b1 = r1.ltrb()
        l2, t2, r2, b2 = r2.ltrb()
        return (l1 <= r2) & (r1 >= l2) & (t1 <= b2) & (b1 >= t2)

    @staticmethod
    def rectCollidesInsideRect(r1: Rectangle, r2: Rectangle) -> bool:
        """
        Cette méthode est similaire à celle ci-dessus mais pour une collision de r1 à l'intérieur de r2 (c'est à dire avec ses bords)
        """
        l1, t1, r1, b1 = r1.ltrb()
        l2, t2, r2, b2 = r2.ltrb()
        return (l1 <= l2) | (r1 >= r2) | (t1 <= t2) | (b1 >= b2)

    @staticmethod
    def rectBounceInsideRect(r1: Rectangle, r2: Rectangle) -> tuple:
        """
        Cette méthode fait dans le même temps le calcul d'un collision de r1 à l'intérieur de r2 et calcule
        le vecteur de rebond de r1 sur les parois de r2.
        la valeur renvoyée est un vecteur unitaire qui ne contient que 1, - 1 ou 0 dans ses composantes
        """
        l1, t1, r1, b1 = r1.ltrb()
        l2, t2, r2, b2 = r2.ltrb()

        return (
            (l1 <= l2) | -(r1 >= r2),
            (t1 <= t2) | -(b1 >= b2)
        )

    @staticmethod
    def rectBounceOutsideRect(r1: Rectangle, r2: Rectangle) -> tuple:
        """
        Cette méthode calcule le vecteur unitaire d'un rebond de r1 à l'extérieur de r2.
        Le calcul part du principe que r1 et r2 sont en collision. Il faut donc le vérifier
        avant d'appeler cette fonction.
        """
        l1, t1, r1, b1 = r1.ltrb()
        l2, t2, r2, b2 = r2.ltrb()

        return (
            -(l1 < l2) | (r1 > r2),
            -(t1 < t2) | (b1 > b2)
        )

    @staticmethod
    def rectCollideInertOusiteRect(r1: Rectangle, v_r1: tuple, r2: Rectangle) -> tuple:
        """
        Cette fonction est appelée à partir du moment ou on sait que r1 est entré en contact avec r2
        depuis l'extérieur. Son rôle est de calculer le vecteur de mouvement résultant de la collision.
        Le résultat est assez simpliste car il se contente de stopper le mouvement (en revoyant 0) en cas
        de collision.
        """
        l1, t1, r1, b1 = r1.ltrb()
        l2, t2, r2, b2 = r2.ltrb()
        r1vx, r1vy = v_r1
        vx = copysign(1, r1vx) if r1vx != 0 else 0
        vy = copysign(1, r1vy) if r1vy != 0 else 0
        if ((l1 < l2) & (r1vx > 0)) | ((r1 > r2) & (r1vx < 0)):
            vx = 0
        if ((t1 < t2) & (r1vy > 0)) | ((b1 > b2) & (r1vy < 0)):
            vy = 0
        return (vx, vy)

    @staticmethod
    def rectCollideInertInsideRect(r1: Rectangle, v_r1: tuple, r2: Rectangle) -> tuple:
        """
        Idem ci-dessus mais pour une collision de r1 à l'intérieur de r2. Ici la collision n'a pas besoin
        d'être vérifiée avant, tout est calculé dans la fonction.
        """
        l1, t1, r1, b1 = r1.ltrb()
        l2, t2, r2, b2 = r2.ltrb()
        r1vx, r1vy = v_r1
        vx = copysign(1, r1vx) if r1vx != 0 else 0
        vy = copysign(1, r1vy) if r1vy != 0 else 0
        if ((l1 <= l2) & (r1vx < 0)) | ((r1 >= r2) & (r1vx > 0)):
            vx = 0
        if ((t1 <= t2) & (r1vy < 0)) | ((b1 >= b2) & (r1vy > 0)):
            vy = 0
        return (vx, vy)
