"""
Ce fichier définit la classe EventsHandler qui comme son nom l'indique
a pour rôle gérer les différents événement survenant à chaque frame
(touches du clavier, souris, boutons de la fenêtre, etc)
"""

import pygame
import sys
from .move_request import MoveRequest

arr_keys_reverse_map = dict()
"""
Cette première série de déclaration est un dictionnaire python assigné à la variable arr_keys_reverse_map
Pygame définit en interne des constantes correspondant au touches du clavier mais elle ne sont pas très
parlantes car ce sont de simples nombres (par ex 16458895321 pour flèche du haut 16458895322 pour flèche de droite)
Ce dictionnaire a pour but d'utilliser ces constantes comme "clé" et de les associer à des valeurs plus
parlantes, donc "up", "right" "left" et "down" qui sont ici les seules touches du clavier dont on s'occupe.
"""
arr_keys_reverse_map[pygame.K_UP] = "up"
arr_keys_reverse_map[pygame.K_RIGHT] = "right"
arr_keys_reverse_map[pygame.K_DOWN] = "down"
arr_keys_reverse_map[pygame.K_LEFT] = "left"


class EventsHandler:
    """
    La classe EventsHandler expose ici 2 méthodes statiques.
    Un méthode statique signifie qu'elle est appelée directement sur la classe et non pas à travers un objet
    instancié à partir de la classe. On peut donc appeler une méthode définie statiquement de cette façon :
    class UneClasse:
        @staticmethod
        def uneMethodeStatique():
            instructions...

        def uneMethodeClassique(self):
            instructions...

    UneClasse.uneMethodeStatique() # ok
    obj = UneClasse()
    obj.uneMethodeStatique() # Erreur
    obj.uneMethodeClassique() # ok
    UneClasse.uneMethodeClassique() # Erreur

    EventsHandler ne contient que 2 méthodes, handleEvtQueue et handleKeyEvent.

    """

    @staticmethod
    def handleEvtQueue(events, moveRequests: MoveRequest):
        """
        handleEvtQueue a pour rôle d'analyser une collection d'énévements survenus pendants cette frame
        et d'effectuer des actions en conséquence. La collection d'événements en question est passés en
        paramètre lors de l'appel de la fonction dans la boucle principale. Les événements sont récupérés
        automatiquement par pygame, et nous les récupérons avec pygame.event.get(). Et nous les retrouvons ici
        représentés par la variable events dans les paramètres de la fonction.
        Ici on ne s'occuppe que de deux type d'événement:
        - L'événement QUIT qui correspond au clic sur le bouton fermer de la fenêtre
        Si un tel événement arrive, on donne l'instruction de quitter le programme immédiatement
        avec sys.exit(), alors le programme arrêtera son execution et la fenêtre se fermera sans condition.
        - Les événements d'appui et de relâchement de touches du clavier
        Si j'appuie sur une touche du clavier, n'importe laquelle, cela fera apparaître un événement KEYDOWN dans
        cette frame, et si je relâche cette touche un événement KEYUP sera déclenché.
        """
        for e in events:
            if e.type == pygame.QUIT or (e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE):
                sys.exit()
            elif (e.type == pygame.KEYDOWN or e.type == pygame.KEYUP):
                EventsHandler.handleKeyEvent(e, moveRequests)

    @staticmethod
    def handleKeyEvent(event: pygame.event, moveRequests: MoveRequest):
        """
        Les événement de touches du clavier sont traités ici un par un.
        Dans cette fonction on récupère un événement qui sera de type soit KEYDOWN soit KEYUP en paramètre ainsi que
        ainsi que la référence (la référence veut dire l'adresse de la mémoire) d'un objet MoveRequest qui est chargé
        d'enregistrer un état des requêtes qui ont été faîtes avec les touches du clavier. Cet état est mis à jour ici
        à travers la méthode setState de l'objet moveRequest et sera utilisé plus loin dans le programme a priori pour
        donner des instruction de mouvement à un sprite.
        Pour savoir la touche du clavier qui a été pressée, on utilise notre dictionnaire définit en haut de page,
        on regarde ce qu'il a pour le code de la touche de l'événement (event.key), si le dictionnaire a quelque chose
        qui correspond à cette constante, il renverra la valeur correspondante ("up" ou "down" etc), sinon il renverra
        "ignore".
        Donc dans la méthode setState de moveRequest, on passe l'expression de récupération dans le dictionnaire, qui est
        exécutée en premier, en 2eme paramètre on passe une expression booléenne qui dit Vrai si la touche est enfoncée, et
        Faux si elle est relâchée. Et ces deux paramètres, une fois calculés sont passés dans l'appel de la fonction setState.
        Donc l'objet moveRequest recevra l'instruction de mettre à jour son état avec une clé qui sera soit "up" "left" "right"
        "down" ou "ignore", et une valeur qui sera soit vrai soit faux. Ainsi on saura s'il y a une requête de mouvement en cours
        pour aller vers le haut, la droite, la gauche ou le bas et nous pourrons y répondre comme nous voulons.

        Le paramètre moveRequests est une liste, cela nous permet de gérer potentiellement plusieurs état de requêtes.
        Par exemple dans le cas d'un multijoueur sur le même ordinateur.
        """
        for req in moveRequests:
            req.setState(
                event.key,
                event.type == pygame.KEYDOWN
            )
