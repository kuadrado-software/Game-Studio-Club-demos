import pygame


class MoveRequest:
    """
    La classe MoveRequest est un modèle d'objet qui a pour but
    de stocker un état de requête de mouvement vers le haut la
    droite la gauche ou le bas. C'est une structure très simple
    avec 4 champs qui permets simplement de pouvoir retrouver ensemble
    ces 4 valeurs en mémoire de façon pratique.
    """

    def __init__(self):
        self.up = False
        self.right = False
        self.down = False
        self.left = False

        self.subscriptions = {
            "up": pygame.K_UP,
            "down": pygame.K_DOWN,
            "right": pygame.K_RIGHT,
            "left": pygame.K_LEFT
        }

        self.updateSubscriptionKeysReverseMap()

    def setState(self, key: int, value):
        """
        Permet de mettre à jour l'état de l'objet afin que celui-ci n'aie pas besoin d'être transormé directement de l'extérieur
        (principe d'encapsulation des objets, cela évite des erreurs, des bugs, des fuites de mémoires etc).
        Cet objet est conçu pour répondre à des requêtes directionnelles assez simples, gauche droite bas haut,
        qui correspondent typiquement à l'appui sur les flèches du clavier pour faire bouger un sprite.
        """
        if key not in self.subscriptionKeysReverseMap.keys():
            return

        direction = self.subscriptionKeysReverseMap[key]
        if direction in self.subscriptions.keys() and key == self.subscriptions.get(direction):
            setattr(self, direction, value)

    def subscribe(self, subscriptions: dict):
        """
        Assigne un objet de souscriptions au champs self.subscriptions.
        L'objet est un dictionnaire avec une clé de direction (up right..) et un code de touche pour chaque entrée.
        ex
        ```
        {
            "up": pygame.K_UP,
            "down": pygame.K_DOWN
        }
        ```
        """
        self.subscriptions = subscriptions
        self.updateSubscriptionKeysReverseMap()

    def updateSubscriptionKeysReverseMap(self):
        """
        Le champs subscriptionKeysReverseMap nous sert à récupérer la clé de la direction (up down left ou right)
        via la constante de la touche du clavier (pygame.K_LEFT etc...)
        Il est mis a jour a chaque changement de l'état général de l'objet.
        """
        rmap = dict()
        for key in self.subscriptions.keys():
            rmap[self.subscriptions[key]] = key

        self.subscriptionKeysReverseMap = rmap
