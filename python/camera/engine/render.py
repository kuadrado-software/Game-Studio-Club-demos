import pygame
from typing import Tuple
from .sprite import Sprite


class Render:
    """
    Expose des méthodes statiques chargées de l'affichage du jeu
    """
    @staticmethod
    def drawGame(surface: pygame.Surface, sprites: Tuple[Sprite, ...], backgroundImage: pygame.Surface, cameraOffset=(0, 0)):
        """
        Methode publique qui appelle les méthodes __drawBackground et __drawSprites de la même classe
        Puis transfère les nouveaux pixels à l'écran grâce à pygame.display.flip()
        """
        Render.__drawBackground(surface, backgroundImage, cameraOffset)
        Render.__drawSprites(surface, sprites, cameraOffset)
        pygame.display.flip()

    @staticmethod
    def __drawBackground(surface, image, cameraOffset):
        """
        Remplit la surface donnée avec la couleur passée en paramètre.
        """
        surface.blit(
            image, (0 + cameraOffset[0], 0 + cameraOffset[1]), image.get_rect())

    @staticmethod
    def __drawSprites(surface, sprites, cameraOffset):
        """
        Dessine l'ensemble des sprites passés en paramètre sur la surface
        """
        for sprite in sprites:
            surface.blit(  # Les pixels sont passé à la surface grâce à la méthode blit()
                sprite.animation.image,
                (sprite.getPosition()[0] + cameraOffset[0],
                 sprite.getPosition()[1] + cameraOffset[1]),
                sprite.animation.getFrameRect()
            )
