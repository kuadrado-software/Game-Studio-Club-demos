import pygame
import os
import json
from .animation import Animation
from .sprite import Sprite


class ResourcesManager:
    """
    Un objet de la classe ResourceManager est responsable du chargement et du stockage en mémoire
    des différentes resources comme les images/animations et les sons. Son rôle sera également de 
    rendre disponibles ces resources pour les parties du programme qui en auront besoin.
    """

    def __init__(self):
        """
        Déclaration des différents champs de l'objet, sans valeur assignée.
        """
        self.animations: tuple
        self.sounds: tuple
        # ...

    def loadAnimations(self):
        """
        Charge les animations à charger en se basant sur la lecture du fichier resources/animations/index.json,
        crée les objets instanciés de type Animation et les stocke dans le champs de l'ojet self.animations.
        """

        # Ouverture du fichier json (json est un format de description de données)
        animationsIndexFile = open(os.path.join(
            "resources", "animations", "index.json"))
        # Lecture du contenu du fichier qui est alors transformé en dictionnaire Python.
        animsIndex = json.load(animationsIndexFile)

        # Déclaration d'une variable 'animations' sous forme d'un dictionnaire vide à laquelle nous ajouterons des entrées ensuite.
        # Ce dictionnaire sera un mirroir de animsIndex mais les données stockées dedans auront été entièrement préparées pour être
        # utilisables directement.
        animations = dict()

        # Initialisation d'une boucle d'itération de toutes les clés du dictionnaire 'animsIndex'
        # Le fichier json qui est à l'origine de ce dictionnaire est construit comme ceci:
        # {
        #     "un_nom_de_sprite": [
        #         {
        #             "name": "courir",
        #             "file": "courir.png",
        #             "frame_count": 12,
        #             "fps": 15,
        #             "dimensions": [423, 315]
        #         },
        #         {
        #             ...
        #         }
        #     ],
        #     "un_autre_sprite": [
        #         {
        #             ...
        #         },
        #         {
        #             ...
        #         }
        #     ]
        # }
        # Les "clés" du dictionnaires que nous parcourons dans cette boucle sont les mots "un_nom_de_sprite", "un_autre_sprite", etc.
        # Nous récupérons la liste des clés d'un dictionnaire grâce à l'appel de la méthode 'keys()'.
        # Chaque entrée du dictionnaire correspondant à une de ces clés contient une liste d'animation ayant chacune un nom, un nom de fichier,
        # un nombre de frames, une vitesse en frame par secondes (fps) et des dimensions largeur hauteur sous forme de liste de deux nombres.
        for key in animsIndex.keys():
            # L'algorithme qui suit est assez compact. Je vais tenter de l'expliquer, mais un équivalent plus découpé est aussi décrit plus bas.

            # "animations" est le dictionnaire vide déclarée plus haut.
            # Nous allons créer une entrée dedans pour y copier les donnée de l'index animsIndex correspondant à la même clé, mais en y apportant
            # quelques modifications.
            animations[key] = (
                # L'expression qui suit est une compréhension de liste Python. Elle permet de créer d'une manière rapide et compacte une liste à partir
                # d'une boucle et d'un calcul dynamique.
                # La syntaxe se synthtise comme ceci
                # [elementmodifié for element in liste]
                # Le résulat obtenu est une liste d'élement modifié à partir de chaque élément nommé element de la liste nommée liste.
                # Bien sur les éléments modifié ne se modifient pas tout seul, il faut donc écrire une expression qui renverra l'élement
                # modifié que l'on veut. Ici l'expression en question sera la création d'un objet de la classe Animation à partir de l'objet
                # d'animation récupéré depuis le fichier de données json.
                [  # Déclaration de la liste finale
                    Animation(  # Création d'une instance d'animation pour chaque élément animData trouvé pour cette clé (key)
                        # Création d'un dictionnaire qui contiendra les entrées de animData + une nouvelle entrée appellée "image"
                        # dans laquelle nous chargeons la resource d'image spécifiée avec pygame.image.load().
                        dict(animData, **{
                            "image": pygame.image.load(
                                os.path.join("resources", "animations",
                                             key, animData["file"])  # Pour charger le fichier nous utilisons l'entrée "file" de l'objet de description de l'animation "animData"
                            )
                        })
                    ) for animData in animsIndex[key]  # Un objet Animation sera ainsi créé pour chaque objet trouvé pour cette clé du dictionnaire.
                ]
            )

        """
        Voici un alogirithme équivalent à celui de ci dessus mais un peu moins compacté

        # Boucle sur chacune des clé du dictionnaire de données de animations
        for key in animsIndex.keys():

            # Déclaration d'une liste vide destinée à contenir les animations décrite dans l'entrée du dictionnaire 
            # animsIndex correspondant à la clé key. (au tour de boucle suivant ce sera donc une nouvelle clé si'il y en a une)
            keyAnimations = list()

            # Création d'une nouvelle boucle qui parcourera la liste d'animations décrite dans cette entrée.
            # Chaque élément sera la description d'une animation
            for animData in animsIndex[key]:

                # Ajoutons une entrée "image" à la description de l'animation dans laquelle nous chargeons la resource image 
                # depuis le fichier. De cette manière l'image sera utilisable par pygame directement et nous n'aurons
                # pas besoin de la charger plus tard.
                animData.update({
                    "image": pygame.image.load(
                        # la fonction os.path.join permet de reconstituer un chemin de fichier de façon sécurisée et indépendante du système d'exploitation
                        os.path.join("resources", "animations",
                                     key, animData["file"])
                    )
                })

                # Une fois la description de l'animation (animData) prete et l'image chargée, nous pouvons créer l'objet Animation à partir de ces données
                newAnim = Animation(animData)

                # Puis nous ajoutons l'objet ainsi obtenu à la liste déclarée en haut de ce tour de boucle (la première boucle).
                keyAnimations.append(newAnim)
            
            # La deuxième boucle terminée, nous pouvons ajouter les éléments obtenus dans keyAnimations à la liste finale "animations"
            animations.extend(keyAnimations)
        """

        # Pour finir copions le contenu de la variable locale animations dans le champs appartenant à l'objet self.animations.
        # De cette manière cette donnée sera accessible pendant toute la durée de vie de l'objet (ou jusqu'à ce qu'on détruise ou remplace
        #  ce champs), alors que la variable locale "animations" ne sera plus valable à la fin de la fonction "loadAnimations()"
        self.animations = animations

    def getAnimationSet(self, key):
        """
        Renvoie la liste d'animation pour la clé indiquée
        """
        return self.animations.get(key)
