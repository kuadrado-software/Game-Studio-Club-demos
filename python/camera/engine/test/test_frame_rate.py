from engine.frame_rate import FrameRateController


class TestFrameRate:
    """
    Test for the FrameRateController class
    """

    def test_nextFrameReady(self):
        """
        Measures the rate given by the fpsController is consistent with a second local control.
        The iterator_limit is an estimation of the number of iteration necessary to apply at least 
        one interval of the fpsController. It might not work for rates under 3 fps.
        """
        iterator_limit = 2 ** 20
        i = 0
        fps = 40
        fpsControler = FrameRateController(fps)
        t = FrameRateController.getTime()
        rate = 0
        while i < iterator_limit:
            if fpsControler.nextFrameReady():
                now = FrameRateController.getTime()
                rate = now - t
                t = now
            i += 1

        assert abs(rate - fpsControler.interval) < 1
