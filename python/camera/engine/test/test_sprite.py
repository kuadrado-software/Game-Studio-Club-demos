import pygame
import os
from engine.sprite import Sprite
from engine.rectangle import Rectangle
from engine.resources_manager import ResourcesManager


class TestSprite:
    """
    Unit tests for the Sprite class
    """

    def getInheritedSpriteObject(self):
        """
        Creates a class inherited on the Sprite class, an returns an instance of this class.
        """
        class CuteSprite(Sprite):
            def __init__(self, animationSet):
                super().__init__(animationSet)
                self.speed = 2
                self.animationSet = animationSet
                self.setAnimation("stand")

        rm = ResourcesManager()
        rm.loadAnimations()
        animSet = rm.getAnimationSet("test")
        aCuteSprite = CuteSprite(animSet)
        return aCuteSprite

    def test_inheritance(self):
        """
        Test if the object created on a class derived on Sprite has the polymorphism Class
        """
        aSprite = self.getInheritedSpriteObject()
        assert isinstance(aSprite, Sprite)

    # TODO, test all methods of the Sprite class through an object of the CuteSprite class
