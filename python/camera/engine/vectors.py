from math import sqrt


def getUnitVec(vector):
    """
    Retourne le vecteur unitaire du vecteur passé en paramètre
    """
    vector_magn = sqrt(vector[0] ** 2 + vector[1] ** 2)
    return (
        vector[0] / vector_magn,
        vector[1] / vector_magn
    ) if vector_magn != 0 else (0, 0)
