from engine import Rectangle


class BigMap:
    def __init__(self, animationSet):
        self.image = animationSet[0].image
        self.rect = Rectangle(self.image.get_rect())
