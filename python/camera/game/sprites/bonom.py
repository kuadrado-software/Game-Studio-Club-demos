import pygame
from engine import Sprite
from engine.vectors import getUnitVec


class Bonom(Sprite):
    def __init__(self, animationSet):
        super().__init__(animationSet)
        self.speed = 10
        self.animationSet = animationSet
        self.setAnimation("face_stand")

    def update(self):
        self.updateAnimation()
        super().update()

    def updateAnimation(self):
        moveReqs = self.moveRequestState
        currentAnim = self.animation.name
        newAnim = currentAnim
        if True in moveReqs.__dict__.values():
            animStates = (
                {
                    "name": "right_run",
                    "value": moveReqs.right
                },
                {
                    "name": "left_run",
                    "value": moveReqs.left
                },
                {
                    "name": "back_run",
                    "value": moveReqs.up
                },
                {
                    "name": "face_run",
                    "value": moveReqs.down
                },
            )
            for anim in animStates:
                if anim["value"]:
                    newAnim = anim["name"]
                    break
        else:
            newAnim = currentAnim.replace("run", "stand")

        if newAnim != currentAnim:
            self.setAnimation(newAnim)
