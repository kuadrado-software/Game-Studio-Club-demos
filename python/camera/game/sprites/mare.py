import pygame
from engine import Sprite


class Mare(Sprite):
    def __init__(self, animationSet):
        super().__init__(animationSet)
        self.speed = 0
        self.animationSet = animationSet
        self.setAnimation("mare")
