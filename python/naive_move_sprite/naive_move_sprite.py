"""
Voici un petit programme très simple pour prendre en main les principe fondammentaux
de la programmation d'un jeu en python. Et plus largement de l'affichage d'objet en mouvement.
On affichera ici simplement un rectangle blanc sur un fond noir se déplaçant vers la droite ou la gauche
"""
import sys
import pygame

"""
Commençons par définir une couleur gris foncé qui nous servira à remplir le fond de la fenêtre.
Une couleur est un tuple de 3 valeurs, rouge vert et bleu. Ce sont des nombres sur 8bits positifs
donc ils peuvent avoir 256 valeurs chacun, de 0 à 255. (1 bit correspond à un chiffre binaire
donc soit 0 soit 1, donc un nombre de 1 bit peut avoir 2 valeurs. Un nombre de 8 bits aura alors
2 puissance 8 valeurs possibles : 2x2x2x2x2x2x2x2 = 256).
Un tuple est une liste de valeurs. Une fois qu'un tuple est défini il ne peut pas être modifié (on ne 
pourra pas changer ce gris foncé en gris clair ou en rouge, les valeurs sont fixées).
Ce tuple est assignée dans une variable appelée DARK_GREY. Un variable est tout simplement une adresse
dans la mémoire de l'ordinateur à laquelle on donne un nom et dans laquelle on met quelque chose, un nombre
des lettres, etc. Cela permet de retrouver facilement et de pouvoir réutiliser notre valeur à plusieurs endroits
par exemple.
"""
DARK_GREY = (30, 30, 30)

"""
pygame.init() est une instruction indispensable pour pouvoir utiliser la biblliothèque pygame.
Un bibliothèque, c'est un ensemble de fichiers de code que nous n'avons pas écrit et que nous importons
(avec l'instruction import que nous voyons plus haut) pour pouvoir nous en servir dans notre propre programme.
Pygame va nous être très utile notamment pour gérer l'affichage de la fenêtre, le fait de pouvoir afficher des
images dedans, de pouvoir intéragir avec la fenêtre grâce à des événements de clavier et de souris etc. Si nous
devions faire tout ça nous même, ce simple programme nous prendrait probablement des mois ou des années à finir !
Donc ici, quand nous appelons la fonction init() de pygame, nous permettons à pygame d'initialiser tout ce qu'il
faut pour pouvoir bénéficier des fonctionnalité de la bibliothèque. Il peut s'agit d'initialisations liées au matériel, 
à l'affichage ou à la création de différentes données, variables, etc...
Si nous n'appelons pas pygame.init() aucune fonction pygame ne marchera.
"""
pygame.init()


"""
Voici 2 nouvelles variables qui vont nous servir à déterminer la largeur et la hauteur de l'écran.
En anglais "screen width" veut dire "largeur de l'écran", et screen height veut dire hauteur de l'écran.
Dans la plupart des programmes les noms données aux variables et aux fonctions sont basés sur l'anglais 
car c'est la langue conventionnelle de l'informatique. Cela permet à tous les informatiens du monde de lire 
un code et de le comprendre, pour permettre un partage des connaissances et des sources le plus international possible
(il est donc recommandé d'apprendre l'anglais pour avancer en informatique)
"""
screenWidth = 1200
screenHeight = 800

"""
Une nouvelle variable, screen, pour "écran". Dans cette variable nous allons stocker une structure de 
données plus complexe qu'un simple nombre, mais nous n'avons pas besoin de trop nous préoccuper de sa 
composition à ce stade.
L'instruction pygame.display.set_mode() va nous renvoyer un objet pratique qui nous permettra de donner des 
instructions à la fenêtre dans laquelle nous afficherons notre jeu, par exemple : 'affiche telle image
à tel endroit', ou encore 'remplis le fond avec telle couleur', etc...
Dans les paramètres de la fonction (entre les parenthèses), nous passons nos deux variable de largeur et hauteur
d'écran. Ainsi l'ojbet screen connaîtra la taille de la fenêtre qu'il devra afficher.
Cette instruction déclenche d'ailleur dans le même temps l'affichage initial de la fenêtre.
"""
screen = pygame.display.set_mode((screenWidth, screenHeight))


"""
C'est le moment de créer notre sprite. Un sprite, c'est le nom générique qu'on donne a tout 
objet graphique affiché dans un programme, doté d'un image, d'une position et d'un certain 
nombre d'autres propriétés. Par exemple des propriétés physiques (masse, puissance motrice etc...).

Ici notre sprite sera un simple rectangle, avec une position initiale au centre de la fenêtre
et une taille de 50x50 pixels. Nous stockons notre sprite dans une variable "sprite", nous pourrons
ainsi lui donner un certain nombre d'instruction tout au long du programme. En l'occurrence nous nous
contenterons de modifier sa position à chaque frame.

Le rectangle qui compose le sprite est un objet générique fourni par pygame qui représente un rectangle :
pygame.Rect(position, taille)
L'objet renvoyé par cette fonction est pratique car il contient des propriété left (gauche), right (droite),
top, bottom, width height (haut bas largeur etc..), et même une méthode move() pour pouvoir le déplacer.
(une méthode est une fonction qui appartient à un objet).

Ici nous donnons une position initiale au sprite au centre de la fenêtre et une taille de 50x50 pixels:
position x = la moitié de la largeur de l'écran moins la moitié de la largeur du sprite = screenWidth / 2 - spriteWidth / 2
position y = la moitié de la hauteur de l'écran moins la moitié de la hauteur du sprite = screenHeight / 2 - spriteHeight / 2
largeur = spriteWidth
hauteur = spriteHeight
"""
spriteWidth = 50
spriteHeight = 50
sprite = pygame.Rect(screenWidth / 2 - spriteWidth / 2,
                     screenHeight / 2 - spriteHeight / 2, spriteWidth, spriteHeight)


"""
On définit une couleur pour le sprite, ici blanc. (rouge, vert et bleu tous au maximum)
Dans un programme plus structuré nous nous serions arrangé pour réunir toutes les données du sprite
dans un seul objet (dimension, position, couleur vitesse, etc). Mais pour commencer il est plus simple 
de créer des variables contenant des valeurs simples.
"""
spriteColor = (255, 255, 255)


"""
Definissons un vecteur de mouvement initial pour notre sprite. 1 en x et 0 en y,
le sprite ira donc de 1 pixels par frame vers la droite, et n'aura pas de mouvement vertical.
Nous définissons cette valeur en 2 temps. D'abord en donnant une vitesse en pixel par frame, ici 1.
Puis en créant un vecteur avec 2 composantes vx et vy, la première donnant la valeur du déplacement
horizontal, et la deuxième la valeur du déplacement vertical.
"""
spriteSpeed = 1
spriteMovement = [spriteSpeed, 0]

while 1:
    """
    while 1 veut dire "Tant que 1 est vrai." A priori 1 sera toujours vrai, car c'est un nombre constant et il est différent de zéro.
    Cette instruction aura donc pour effet de lancer une boucle infinie. Ça veut dire que l'ensemble des instructions contenues dans cette boucle
    se répèteront indéfiniement, jusqu'à ce qu'une instruction ordonne au programme de s'arrêter.
    """

    for event in pygame.event.get():
        """
        Ceci est également une boucle, mais pas infinie cette fois. Cette boucle parcoure l'ensemble des différents
        événements surevenus pendant cette frame (clic de souris, boutons de la fenêtre, touches du clavier etc). Les
        événements sont intercepté dans le code de pygame, nous avons donc pas à nous en occuper. Nous les récupérons
        sous forme d'une liste grâce à pygame.event.get() donc `for event in pygame.event.get()` veut dire:
        "Pour chaque évenement que nous nommerons 'event' dans la liste donnée par pygame.event.get(), executer l'instruction
        suivante:
        Et ici l'instruction en question est:
        si le type de l'événement est égal à la constante pygame.QUIT (pygame.QUIT est un nombre donné par pygame et stocké dans
        une variable par pygame sous le nom QUIT), quitter le programme immédiatement.

        L'événement QUIT survient lorsque nous cliquons sur le bouton fermer de la fenêtre. Ou alors si nous programmons notre propre
        bouton quitter.
        """
        if event.type == pygame.QUIT:
            sys.exit()

    """
    C'est le moment d'utiliser notre couleur de fond d'écran définie plus haut.
    Nous utilisons notre objet screen (qui représente la fenêtre) et nous lui donnons
    l'instruction de se remplir de la couleur DARK_GREY(30,30,30) grâce à sa méthode `fill()`
    """
    screen.fill(DARK_GREY)

    """
    La méthode draw de pygame permet de dessiner des formes primitives sur une surface.
    Là on appelle la methode rect() car nous voulons dessiner un rectangle, et nous passons en paramètre 
    la surface sur laquelle nous voulons afficher le rectangle (screen), la couleur du rectangle à dessiner, 
    et enfin  notre objet sprite qui n'est rien d'autre qu'un rectangle. (Si nous mettions autre chose qu'un rectangle
    dans cette fonction elle nous renverrait une erreur et le programme s'arrêterait)
    """
    pygame.draw.rect(screen, spriteColor, sprite)

    """
    Appliquons le vecteur de mouvement "spriteMovement" sur le sprite. 
    Pygame fournit une méthode move() applicable sur un rectangle pour pouvoir faire ça facilement. 
    La methode move renvoie un nouveau rectangle placé aux nouvelles coordonnées. Nous réassignons 
    ce nouveau rectangle à notre variable sprite afin que la nouvelle valeur soit prise en compte à 
    l'affichage et perdure dans la prochaine frame.
    """
    sprite = sprite.move(spriteMovement)

    """
    Afin que le programme soit un peu plus amusant à regarder nous allons faire sorte que la direction
    change lorsque le sprite aura atteint le bord de l'écran
    """
    if sprite.right >= screenWidth:
        """
        si la coordonnée du bord droit du rectangle à atteint une valeur égale à la largeur de la fenêtre
        La direction de la composante horizontale du vecteur doit être négative afin d'aller vers la gauche
        """
        spriteMovement[0] = -spriteSpeed
    elif sprite.left <= 0:
        """
        sinon, si la gauche du sprite a passé le bord gauche de l'écran (qui est à 0)priteMovement[0]
        La direction horizontale du vecteur redevient positive afin d'aller vers la droite
        """
        spriteMovement[0] = spriteSpeed

    """
    La methode flip permet d'appliquer les pixels qui ont été passé à la surface "screen"
    (qui est une surface "virtuelle") sur l'affichage matériel (notre écran)
    """
    pygame.display.flip()
