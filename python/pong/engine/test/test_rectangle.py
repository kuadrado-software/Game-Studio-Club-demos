import pygame
from engine import Rectangle


class TestRectangle:
    """
    Tests for the Rectangle class
    """

    def getTestRectangle(self, x, y, w, h):
        """
        Creates a rectangle for testing purpose. This function is not a test
        """
        return Rectangle(pygame.Rect((x, y, w, h)))

    def test_init(self):
        """
        Test initialization of the rectangle attributes
        """
        r = self.getTestRectangle(10, 12, 40, 64)
        assert r.left == 10
        assert r.top == 12
        assert r.width == 40
        assert r.height == 64
        assert r.right == 50
        assert r.bottom == 76

    def test_getPosition(self):
        """
        Test getPosition method
        """
        r = self.getTestRectangle(34, 22, 50, 50)
        assert r.getPosition()[0] == 34 and r.getPosition()[1] == 22

    def test_setPosition(self):
        """
        Test setPosition method
        """
        r = self.getTestRectangle(0, 0, 50, 50)
        r.setPosition(40, 23)
        assert r.left == 40
        assert r.top == 23
        assert r.width == 50
        assert r.height == 50
        assert r.right == 90
        assert r.bottom == 73

    def test_ltrb(self):
        """
        Test ltrb method
        """
        r = self.getTestRectangle(12, -50, 34, 22)
        left, top, right, bottom = r.ltrb()
        assert left == 12
        assert top == -50
        assert right == 46
        assert bottom == -28

    def test_xywh(self):
        """
        Test xywh method
        """
        r = self.getTestRectangle(0, 20, 34, 22)
        x, y, width, height = r.xywh()
        assert x == 0
        assert y == 20
        assert width == 34
        assert height == 22

    def test_size(self):
        """
        Test the size method
        """
        r = self.getTestRectangle(0, 0, 50, 100)
        size = r.size()
        assert size[0] == 50 and size[1] == 100

    def test_getPygameRect(self):
        """
        Test if the getPygameRect method return an instance of pygame.Rect
        """
        r = self.getTestRectangle(0, 0, 50, 100)
        pr = r.getPygameRect()
        assert isinstance(pr, pygame.Rect)
