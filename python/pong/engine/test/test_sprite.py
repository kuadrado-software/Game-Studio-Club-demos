import pygame
import os
from engine.sprite import Sprite
from engine.rectangle import Rectangle


class TestSprite:
    """
    Unit tests for the Sprite class
    """

    def getInheritedSpriteObject(self):
        """
        Creates a class inherited on the Sprite class, an returns an instance of this class.
        """
        class CuteSprite(Sprite):
            def __init__(self):
                super().__init__()
                self.image = pygame.image.load(os.path.join(
                    "resources", "images", 'ball.png'))
                self.boundingBox = Rectangle(self.image.get_rect())
                self.speed = 4

        aCuteSprite = CuteSprite()
        return aCuteSprite

    def test_inheritance(self):
        """
        Test if the object created on a class derived on Sprite has the polymorphism Class
        """
        aSprite = self.getInheritedSpriteObject()
        assert isinstance(aSprite, Sprite)

    # TODO, test all methods of the Sprite class through an object of the CuteSprite class