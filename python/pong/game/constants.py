"""
Ce module ne fait que définir un certain nombre de constantes dans un dictionnaire.
Ces valeurs nous seront utiles un peu partout dans le code. Ce fichier permet de centraliser
la configuration du jeu.
"""
game_constants = {
    "distance_wall_paddle": 200,
    "ball_size": (8, 8),
    "paddle_size": (14, 50),
    "ball_initial_speed": 4,
    "paddle_speed": 16,
    "ball_service_delay": 1000,  # milliseconds
    "out_of_field_wall_distance": 80,
    "colors": {
        "black": (0, 0, 0),
        "white": (255, 255, 255)
    }
}
