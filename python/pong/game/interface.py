import pygame
import os
from game import game_constants


class Interface:
    """
    Ce module est responsable de la création et de l'affichage des éléments d'interface du jeu.
    À savoir, le filet de séparation du terrain qui est représenté par une ligne verticale en pointillé, 
    et l'affichage du score.
    """

    def __init__(self):
        """
        Initialise le module d'affichage de texte de pygame avec pygame.font
        et charge la police de caractères depuis de fichier resources/fonts/pixel_square.ttf
        Nous utiliserons cette configuration pour l'affichage du score.
        """
        pygame.font.init()
        self.font = pygame.font.Font(os.path.join(
            "resources", "fonts", 'pixel_square.ttf'), 100)

    def drawNet(self, surface, screenRect):
        """
        Dessine la ligne en pointillé au centre du terrain pour représenter le filet.
        """
        dash_length = 8
        dash_interval = 8
        x = screenRect.width / 2
        y = 0
        while y < screenRect.height:
            pygame.draw.line(
                surface,
                (255, 255, 255),
                (x, y),
                (x, y + dash_length)
            )
            y = y + dash_length + dash_interval

    def drawScore(self, score, surface, screenRect):
        """
        Dessine le score de chaque joueur.
        """
        txt_score_p1 = str(score.get("player_1"))
        txt_score_p2 = str(score.get("player_2"))

        render_score_player_1 = self.font.render(
            txt_score_p1,
            False,
            game_constants["colors"]["white"]
        )

        text_width = self.font.size(txt_score_p1)[0]
        text_pos_x = screenRect.width / 4 - text_width / 2
        surface.blit(render_score_player_1, (text_pos_x, 50))

        render_score_player_2 = self.font.render(
            txt_score_p2,
            False,
            game_constants["colors"]["white"]
        )

        text_width = self.font.size(txt_score_p2)[0]
        text_pos_x = (3 * screenRect.width) / 4 - text_width / 2
        surface.blit(render_score_player_2, (text_pos_x, 50))
