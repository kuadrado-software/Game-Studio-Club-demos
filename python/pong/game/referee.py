from .constants import game_constants
from engine import FrameRateController
from .sounds import SoundPlayer

GAME_PLAYING = 0
SERVING_PLAYER_1 = 1
SERVING_PLAYER_2 = 2


class Referee:
    """
    L'arbitre du jeu. L'objet qui sera créé sur la classe Referee permettra de contrôler l'état du jeu
    en fonction de la position de la balle, de mettre à jour le score des joueurs, et de reservir la
    balle quand elle sort
    """

    def __init__(self):
        self.time = FrameRateController.getTime()
        self.gameState = GAME_PLAYING

    def arbitrate(self, ball, screenRect, score):
        """
        Detecte si la balle est out pendant le jeu. Sinon, gère le service de la balle.
        """
        if self.gameState == GAME_PLAYING:
            if ball.boundingBox.left <= screenRect.left + game_constants["out_of_field_wall_distance"]:
                self.gameState = SERVING_PLAYER_2
                score["player_2"] += 1
                self.reset()
            elif ball.boundingBox.right >= screenRect.right - game_constants["out_of_field_wall_distance"]:
                self.gameState = SERVING_PLAYER_1
                score["player_1"] += 1
                self.reset()
        else:
            self.serve(self.gameState, ball, screenRect)

    def reset(self):
        '''
        Met le timer à jour pour le calcul du délai de service de la balle.
        Joue le son de balle hors jeu.
        '''
        SoundPlayer.play("lose")
        self.time = FrameRateController.getTime()

    def serve(self, toPlayer: int, ball, screenRect):
        """
        Vérifie l'écoulement du délai de service de la balle.
        Si il est écoulé, la balle est relancée avec une vitesse initiale.
        Sinon la balle est retenue en dehors de l'écran.
        """
        if self.readyToServe():
            ball.setPosition(screenRect.width / 2, 0)
            ball.setSpeed(game_constants["ball_initial_speed"])
            direction = -1 if toPlayer == 1 else 1
            ball.setSpeedVector((direction, 1))
            self.gameState = GAME_PLAYING

        else:
            # Tant que l'arbitre n'est pas prêt à servir la balle, nous la retenons en dehors du terrain
            ball.setPosition(-50, -50)

    def readyToServe(self):
        """
        Renvoie Vrai si le délai de service de la balle est écoulé.
        """
        now = FrameRateController.getTime()
        elapsed = now - self.time
        ready = elapsed > game_constants["ball_service_delay"]
        if ready:
            self.time = now
        return ready
