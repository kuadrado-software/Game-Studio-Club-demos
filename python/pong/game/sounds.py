from pygame import mixer


class SoundPlayer:
    """
    Cette classe expose des méthode statiques pour jouer les sons chargés dans la méthode init
    Tout appel de la fonction play si la fonction init n'a pas été appelée provoquera une erreur.
    Tout appel de la fonction play avec en paramètre autre chose que les clé définies dans le champs
    SoundPlayer.sounds entrainera également une erreur (KeyError).
    """
    @staticmethod
    def init():
        mixer.init()
        SoundPlayer.sounds = {
            "ping": mixer.Sound("resources/sounds/ping.wav"),
            "pong": mixer.Sound("resources/sounds/pong.wav"),
            "lose": mixer.Sound("resources/sounds/lose.wav")
        }

    @staticmethod
    def play(soundName: str):
        mixer.Sound.play(SoundPlayer.sounds.get(soundName))
