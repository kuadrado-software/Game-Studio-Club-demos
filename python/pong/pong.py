# Reproduction du célèbre jeu vidéo Pong (Atari 1972).

import sys
import pygame
import random

from engine import Rectangle
from engine import MoveRequest
from engine import EventsHandler
from engine import FrameRateController

from sprites import Ball
from sprites import Paddle
from game import Interface
from game import game_constants
from game import Referee
from game import SoundPlayer

# Initialisation de pygame
pygame.init()

# initialisation du module de sons
SoundPlayer.init()

# Définition d'un rectangle correspondant à la taille de la fenêtre que l'on veut
screenRect = Rectangle(pygame.Rect(0, 0, 1400, 800))

# Création de la fenêtre
screen = pygame.display.set_mode(screenRect.size())

# Création du sprite ball et initialisation de sa vitesse et sa position
ball = Ball()
ball.setPosition(screenRect.width / 2, 0)
# Choisions une direction aléatoire vers la droite ou la gauche pour démarrer la partie
ball.setSpeedVector((random.choice((1, -1)), 1))

# Création des sprites paddle_1 et paddle_2 (les raquettes) et de leur position.
paddle_1 = Paddle()
paddle_1.setPosition(
    game_constants.get("distance_wall_paddle"),
    screenRect.height / 2 - paddle_1.boundingBox.height / 2
)

paddle_2 = Paddle()
paddle_2.setPosition(
    screenRect.width -
    game_constants.get("distance_wall_paddle") - paddle_2.boundingBox.width,
    screenRect.height / 2 - paddle_2.boundingBox.height / 2
)

# Nous gérerons le score à l'aide d'un dictionnaire,
# une entrée sera pour le score du joueur 1 et l'autre pour le joueur 2
score = {
    "player_1": 0,
    "player_2": 0
}

# Créer l'interface du jeu, cet à dire tout les éléments qui forment le terrain de jeu,
# le filet de séparation et l'affichage du score
interface = Interface()

# Création de l'objet eventHandler qui nous permettra de gérer les événements de clavier entre autre.
eventsHandler = EventsHandler()

# création de l'objet moveRequest_p1 qui enregistrera les entrées clavier pour le joueur 1
moveRequest_p1 = MoveRequest()
# En appelant la methode subscribe nous redéfinissons les directions auxquelles l'objet moveRequest
# obéira. Comme nous ne passons que up et down en paramètre, seul les requêtes de mouvement
# verticaux seront pris en compte.
# Pour le joueur un nous associons les direction au touches du clavier control et shift gauche pour le bas et le haut
moveRequest_p1.subscribe({
    "up": pygame.K_LSHIFT,
    "down": pygame.K_LCTRL
})

# Même chose pour le joueur 2, mais nous associeront les requêtes au touches flèche du haut et flèche du bas
moveRequest_p2 = MoveRequest()
moveRequest_p2.subscribe({
    "up": pygame.K_UP,
    "down": pygame.K_DOWN,
})

# Attribuons les état de requêtes à leur sprite respectif
paddle_1.setMoveRequestState(moveRequest_p1)
paddle_2.setMoveRequestState(moveRequest_p2)

# Créeons notre arbitre ! L'objet Referee jouera le rôle de l'arbitre, il observera l'état des objet
# du jeu et s'occupera de mettre à jour le score et de relancer la balle si elle sort
referee = Referee()

# Création de l'objet fpsController qui permettra de cadencer la vitesse de la boucle de rendu ci-dessous.
fpsController = FrameRateController(50)

while 1:
    if fpsController.nextFrameReady():  # On verifie si l'interval donné par le fpsController est écoulée
        # Si oui on calcule la frame suivante

        # On repeint le fond en noir
        screen.fill(game_constants["colors"]["black"])

        # On passe les événements au eventsHandler qui so'ccupera de mettre à jour
        # l'objet moveRequest si une flèche à été appuyée ou relâchée.
        eventsHandler.handleEvtQueue(
            pygame.event.get(),
            (paddle_1.moveRequestState, paddle_2.moveRequestState)
        )

        for paddle in (paddle_1, paddle_2):
            # Appliquons a chaque raquette l'état de requêtes de mouvements que nous luis avons assigné
            paddle.applyMoveRequestState()
            paddle.collideInside(screenRect)
            paddle.move()
            pygame.draw.rect(
                screen, game_constants["colors"]["white"], paddle.boundingBox.getPygameRect())
            pygame.draw.rect(
                screen, game_constants["colors"]["white"], paddle.boundingBox.getPygameRect())
            # Faisons rebondir la balle sur chacune des 2 raquettes en cas de collision
            ball.bouncePaddle(
                paddle.boundingBox,

                # En cas de collision cette fonction sera appliquée. Elle aura pour effet d'accélerer un peu la balle
                # a chaque fois qu'elle touchera une raquette. Une "lambda" est un type de fonction particulier.
                # Ce sont des fonction "anonymes", elle n'ont pas besoin d'être déclarées (donc elle n'ont pas besoin de nom),
                # elle peuvent être passées à d'autres fonctions et exécutées plus tard, tout comme nous passons d'autres données
                # comme des nombres ou des caratères qui sont elles aussi destinées à être utilisées par la fonction à laquelle
                # elles sont passées.
                lambda: ball.setSpeed(ball.speed + 1)
            )

        # On calcule la collision et le rebond de la balle sur le bord de l'écran, si la balle ne touche pas le bord de l'écran
        # il ne se passera rien dans cette frame.
        ball.bounceInsideWalls(screenRect)
        # On applique les vecteurs de mouvement calculés pour la balle.
        ball.move()

        # L'arbitre a le dernier mot sur le calcul de la position de la ball, donc on execute sa fonction en dernier,
        # avant de redessiner la balle.
        referee.arbitrate(ball, screenRect, score)

        # On redessine la balle a sa nouvelle position
        pygame.draw.rect(
            screen, game_constants["colors"]["white"], ball.boundingBox.getPygameRect())

        interface.drawNet(screen, screenRect)
        interface.drawScore(score, screen, screenRect)

        # On transfère les pixels de la fenêtre à l'affichage matériel, les nouveaux remplacent les anciens,
        # les anciens sont supprimés pour laisser leur place en mémoire.
        pygame.display.flip()
