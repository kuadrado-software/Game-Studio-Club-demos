import pygame
from engine import Rectangle
from engine import Sprite
from game import game_constants
from game import SoundPlayer
from engine.collisions import CollisionHandler
from engine.vectors import getUnitVec


class Ball(Sprite):
    """
    La classe Ball est une extension de la classe Sprite. L'héritage est spécifié avec la notation
    Ball(Sprite) qui définit donc un modèle d'objet Ball héritant d'un modèle Sprite.
    La objet de la classe Ball contiendra tous les attributs et les méthodes définis par la
    classe Sprite et a la possibilité de les redéfinir à volonté.
    Ici la classe ball ne fait que définir les champs boudingBox et speed qui ne sont
    définis dans la classe Sprite que par leur type. Le champs image n'est pas défini car nous ne
    l'utilisons pas dans Pong.
    """

    def __init__(self):
        super().__init__()
        self.boundingBox = Rectangle(pygame.Rect(
            (0, 0), game_constants.get("ball_size")))
        self.speed = game_constants.get("ball_initial_speed")

    def bouncePaddle(self, rect, callBack=lambda: 0):
        """
        La méthode bounce proposée par l'héritage de la classe Sprite est un
        peu trop simple pour le pong car elle ne calcule que des rebonds symétriques.
        Nous allons donc réécrire une méthode spécifique ici afin de la rendre un peu plus fidèle au
        jeu d'origine en tenant compte de si on renvoie la balle avec le millieu de la raquette ou avec un bord.
        """
        if CollisionHandler.rectCollidesOutsideRect(self.boundingBox, rect):
            # Récupérons le vecteur qui aurait été calculé normalement
            bounceVector = CollisionHandler.rectBounceOutsideRect(
                self.boundingBox,
                rect
            )

            # et customisons le un peu en anulant le mouvement vertical si le rebond a lieu au centre
            # de la raquette
            paddleCenter = rect.top + rect.height / 2
            ballCenter = self.boundingBox.top + self.boundingBox.height / 2
            offset = 8  # 3 pixel de tolérance pour détecter le centre
            hitCenter = abs(ballCenter - paddleCenter) < offset

            vx, vy = self.speedVector
            if hitCenter:
                # Traitons notre cas spécial de rebond horizontal si onn tombe au centre de la raquette
                vy = 0
            else:
                hitTop = ballCenter - paddleCenter < 0
                vy = -self.speed if hitTop else self.speed

            self.setSpeedVector(getUnitVec((
                bounceVector[0] * abs(vx) or vx,
                bounceVector[1] * abs(vy) or vy
            )))

            # Jouer le son qui correspond au rebond sur la raquette
            SoundPlayer.play("ping")

            # Le paramètre callBack nous permet de passer une fonction en paramètre afin de définir
            # un comportement customisé en cas de collision, en plus du rebond
            callBack()

    def bounceInsideWalls(self, rect):
        """
        La methode bounceInside de la calsse Sprite fait dans le même temps le calcul de la collision et du vecteur
        du rebond mais nous avons besoins d'y intercaler le déclenchement du son en cas de collision.
        Nous allons donc réécrire une fonction spécifique en nous inspirant de la fonction de base.

        En python nous pouvons définir des fonctions dans des fonctions, comme des sous fonctions.
        C'est pratique pour structurer le code tout en gardant isolée des parties qui ne servent qu'à
        seul endroit.
        """

        def bounce(r1, r2):
            # Ceci est une copie de la fonction CollisionHandler.rectBounceInsideRect(rect1, rect2)
            # Nous la customisons pour y intégrer notre son.
            l1, t1, r1, b1 = r1.ltrb()
            l2, t2, r2, b2 = r2.ltrb()

            collidesTop = t1 <= t2
            collidesBottom = b1 >= b2
            collidesLeft = l1 <= l2
            collidesRight = r1 >= r2

            if collidesTop | collidesRight | collidesBottom | collidesLeft:
                SoundPlayer.play("pong")

            return (
                collidesLeft | -collidesRight,
                collidesTop | -collidesBottom
            )

        self.applyBounceVector(
            bounce(self.boundingBox, rect)
        )
