import pygame
from engine import Rectangle
from engine import Sprite
from game import game_constants


class Paddle(Sprite):
    """
    Tout comme la classe Ball la classe Paddle hérite de la classe Sprite mais on
    l'initialise avec des valeurs un peu différentes dans ses attributs.
    Ici on n'initialize pas non plus le champs "image", car nous n'utiliserons que 
    les valeur de boundingBox pour dessiner le rectangle correspondant.
    """

    def __init__(self):
        super().__init__()
        self.boundingBox = Rectangle(pygame.Rect(
            (0, 0), game_constants.get("paddle_size")))
        self.speed = game_constants.get("paddle_speed")
