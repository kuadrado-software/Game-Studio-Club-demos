# Ici on importe dans notre fichier les différentes choses dont on
# va avoir besoin. Certains imports sont des bibliothèques standard
# intégrées dans le langage com sys.
# D'autres ont été installées en plus sur l'ordinateur, c'est le cas de pygame.
# Et d'autres imports concernent simplement des fichiers (on les appelle modules)
# présents dans le même dossier comme Rectangle ou Ball... Cela permet de découper
# le code en plusieurs fichiers afin de le rendre plus lisible.
import sys
import pygame

from engine import Rectangle
from engine import MoveRequest
from engine import EventsHandler
from engine import FrameRateController

from ball import Ball

# On stock la couleur BLACK dans une variable afin de pouvoir
# la réutiliser ailleurs sans avoir à la réécrire "en dur".
# Une couleur est un tuple de 3 valeurs (rouge vert et bleu) entre 0 et 255
# (ce sont des nombres de 8bits donc ils n'ont que 256 valeurs possible chacun)
# Un tuple est une liste de valeurs, il ne peut pas être modifié après avoir été déclaré
BLACK = (0, 0, 0)

# La bibliothèque pygame a un certain de nombre de choses cachées
# à initialiser avant qu'on puisse s'en servir, il faut donc appeler
# pygame.init() avant quoi que ce soit d'autre dans pygame.
pygame.init()

# On stock en mémoire un rectangle correspondant au dimensions de la fenêtre
# qu'on veut afficher
screenRect = Rectangle(pygame.Rect(0, 0, 1200, 800))

# On crée une fenêtre avec pygame.display en lui passant les dimensions de notre rectangle screenRect
screen = pygame.display.set_mode(screenRect.size())

# On crée notre sprite d'après le modèle Ball importé de ball.py (voir en haut "from ball import Ball"),
# et on initialise sa position au centre de la fenêtre
sprite = Ball()
sprite.setPosition(
    screenRect.width / 2 - sprite.boundingBox.width / 2,
    screenRect.height / 2 - sprite.boundingBox.height / 2
)

# L'objet moveRequest est contruit d'après le modèle MoveRequest importé de move_request.py et il
# nous permettra de stocker en mémoire un état des instruction de mouvements qu'on a donné.
# Par exemple, si j'appuie sur la flèche du haut je vais enregistrer dans cet objet que je veut aller vers le haut.
# Si je relache le bouton, l'objet enregistrera que ce n'est plus le cas.
# L'état général de l'objet sera ensuite utilisé pour appliquer le mouvement souhaité.
moveRequest = MoveRequest()

# L'objet fpsController permet de limiter la vitesse de rafraichissement du jeu à un certain nombre de
# frames par secondes. Dans la boucle principale (le 'while 1:'), on appellera la methode 'nextFrameReady()'
# de cet objet afin de savoir si on peut passer à la frame suivante. En interne l'objet vérifie que l'interval
# donné au départ a fini de s'écouler ou non. Voir frame_rate.py
fpsController = FrameRateController(50)

# La boucle principale de rendu. Elle est nécessaire à n'importe quel programme qui veut pouvoir
# afficher des objets en mouvement. C'est une boucle infinie car elle dit "while 1:" ce qui veut dire
# "Tant que 1 est vrai", bien sûr 1 est toujours vrai car c'est un nombre constant et il n'est pas zero,
# donc la boucle tournera pour toujours. Jusqu'à ce qu'un instruction ordonne au programme de se fermer.
while 1:
    # On ne calcule la frame suivante que si l'interval du fpsController s'est écoulé
    if fpsController.nextFrameReady():

        # La classe EventHandler importée via le fichier events_handler.py contient des fonctions qui
        # nous permettent de gérer des événements tels que l'appui sur les touches du clavier,
        # des mouvement ou clics de souris, un click sur le bouton fermer de la fenêtre, etc.
        # La fonction EventsHandler.handleEvtQueue va traiter les différents événement qui ont lieu pendant cette frame
        # On lui passe une référence de l'objet moveRequest pour qu'il puisse le mettre à jour en conséquence
        EventsHandler.handleEvtQueue(pygame.event.get(), moveRequest)

        # Le eventsHandler va traiter les différents événement qui ont lieu pendant cette frame
        # On lui passe une référence de l'objet moveRequest pour qu'il puisse le mettre à jour en conséquence
        sprite.applyMoveRequest(moveRequest)

        # Pour le notre sprite ne puisse pas sortir de la fenêtre on appelle sa methode collideInside(rect)
        # La vitesse du sprite sera recalculée en fonction de cette contrainte
        sprite.collideInside(screenRect)

        # On appelle la methode move() du sprite pour appliquer le
        # calcul fait sur sa vitesse (son vecteur de mouvement) à sa position
        sprite.move()

        # On appelle la methode fill sur l'objet screen de pygame en lui passant une couleur en paramètre
        # Cela aura pour effet de remplir le fond de la fenêtre de noir
        screen.fill(BLACK)

        # La methode blit permet de copier les pixel d'une image dans l'objet screen à une certaine position
        screen.blit(sprite.image, sprite.getPosition())

        # On applique les nouveau pixels dessinés sur l'affichage matériel, les anciens pixels sont remplacés par les nouveaux
        pygame.display.flip()

        # Et on repart pour un tour !
